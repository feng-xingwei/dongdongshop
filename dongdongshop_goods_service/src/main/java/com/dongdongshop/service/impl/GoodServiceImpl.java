package com.dongdongshop.service.impl;

import com.alibaba.fastjson.JSON;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.mapper.TbGoodsDescMapper;
import com.dongdongshop.mapper.TbGoodsMapper;
import com.dongdongshop.mapper.TbItemMapper;
import com.dongdongshop.pojo.TbGoods;
import com.dongdongshop.pojo.TbGoodsDesc;
import com.dongdongshop.pojo.TbGoodsExample;
import com.dongdongshop.pojo.TbItem;
import com.dongdongshop.service.GoodService;
import javassist.expr.NewArray;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.Array;
import java.util.Date;
import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class GoodServiceImpl implements GoodService {

    @Resource
    private TbGoodsMapper goodsMapper;

    @Resource
    private TbGoodsDescMapper goodsDescMapper;

    @Resource
    private TbItemMapper itemMapper;

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    //增加商品
    @Override
    public Result addGood(TbGoods good, TbGoodsDesc goodsDesc, BigDecimal[] price, Integer[] number, String titile2) {
        try {
            goodsMapper.insertSelective(good);
            goodsDesc.setGoodsId(good.getId());
            int i = goodsDescMapper.insertSelective(goodsDesc);

            String[] strArr = {"移动3G 16G", "移动3G 32G", "移动4G 16G", "移动4G 32G"};
            String[] strArr2 = {"{\"网络\":\"移动3G\",\"机身内存\":\"16G\"}", "{\"网络\":\"移动3G\",\"机身内存\":\"32G\"}",
                    "{\"网络\":\"移动4G\",\"机身内存\":\"16G\"}", "{\"网络\":\"移动4G\",\"机身内存\":\"32G\"}"};
            for(int j = 0; j < price.length; j++){
                TbItem item = new TbItem();
                item.setTitle(good.getGoodsName() + " " + strArr[j]);
                item.setPrice(price[j]);
                item.setStockCount(number[j]);
                item.setNum(number[j]);
                item.setCategoryid(good.getCategory3Id());
                item.setGoodsId(good.getId());
                item.setSellerId(good.getSellerId());
                item.setCreateTime(new Date());
                item.setUpdateTime(new Date());
                item.setSpec(strArr2[j]);
                itemMapper.insertSelective(item);
            }

            if(i <= 0){
                return Result.build(ConstantEnum.ERROR_CREATE);
            }
        }catch (Exception e){
            e.printStackTrace();
            return Result.build(ConstantEnum.ERROR_CREATE);
        }
        return Result.build(ConstantEnum.SUCCESS_CREATE);
    }

    //商家展示
    @Override
    public Result getGoodList(String sellerId, TbGoods goods) {
        String auditStatus = goods.getAuditStatus();
        String goodsName = goods.getGoodsName();

        TbGoodsExample tbGoodsExample = new TbGoodsExample();
        TbGoodsExample.Criteria criteria = tbGoodsExample.createCriteria();
        criteria.andIsDeleteNotEqualTo("0");
        criteria.andSellerIdEqualTo(sellerId);
        if(!"".equals(auditStatus) && auditStatus != null){
            criteria.andAuditStatusEqualTo(auditStatus);
        }
        if(!"".equals(goodsName) && goodsName != null){
            criteria.andGoodsNameLike("%" + goodsName + "%");
        }
        List<TbGoods> tbGoods = goodsMapper.selectByExample(tbGoodsExample);
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(tbGoods);
    }

    //后台运营商展示
    @Override
    public Result getAllGoodList(TbGoods goods) {

        String auditStatus = goods.getAuditStatus();
        String goodsName = goods.getGoodsName();

        TbGoodsExample tbGoodsExample = new TbGoodsExample();
        TbGoodsExample.Criteria criteria = tbGoodsExample.createCriteria();
        criteria.andIsDeleteNotEqualTo("0");
        if(!"".equals(auditStatus) && auditStatus != null){
            criteria.andAuditStatusEqualTo(auditStatus);
        }
        if(!"".equals(goodsName) && goodsName != null){
            criteria.andGoodsNameLike("%" + goodsName + "%");
        }
        List<TbGoods> tbGoods = goodsMapper.selectByExample(tbGoodsExample);
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(tbGoods);
    }

    //批量审核
    @Override
    public Result updateBatch(String[] ids, String status) {
        goodsMapper.updateBatch(ids, status);
        if(status.equals("1")){
            System.out.println("生产者发送消息");
            for(int i = 0; i < ids.length; i++){
                rocketMQTemplate.convertAndSend("myGroup", ids[i]);
            }
        }
        return Result.build(ConstantEnum.SUCCESS_UPDATE);

    }

    //批量删除
    @Override
    public Result deleteBatch(String[] ids) {
        goodsMapper.updateBatchDelete(ids);
        return Result.build(ConstantEnum.SUCCESS_DELETE);
    }

    //根据商品id查询商品
    @Override
    public TbGoods getGoodByGoodId(Long goodId) {
        return goodsMapper.selectByPrimaryKey(goodId);
    }

    //根据商品id查询商品详细信息
    @Override
    public TbGoodsDesc getGoodDescByGoodId(Long goodId) {
        return goodsDescMapper.selectByPrimaryKey(goodId);
    }
}
