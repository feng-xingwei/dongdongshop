package com.dongdongshop.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.pojo.Cart;
import com.dongdongshop.pojo.TbItem;
import com.dongdongshop.pojo.TbOrderItem;
import com.dongdongshop.service.CartService;
import com.dongdongshop.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class CartServiceImpl implements CartService {

    @Reference
    private ItemService itemService;

    @Autowired
    private RedisTemplate redisTemplate;

    //增加购物车信息
    @Override
    public List<Cart> addCart(List<Cart> cartList, Long itemId, Integer num) {
        //1.根据itemId查询SKU单品-item
        TbItem item = itemService.getItemByItemId(itemId);
        //2.根据item获取商家Id
        String sellerId = item.getSellerId();
        //3.根据商家Id判断当前购物车中有没有当前这个商家的数据
        Cart cart = findCartBySellerId(cartList, sellerId);
        if(cart == null){//4.购物车中没有该商品的商家
            //4.1创建一个该商家
            cart = new Cart();
            //4.2将商品信息添加到商家中
            cart.setSellerId(sellerId);
            cart.setSeller(item.getSeller());
            List<TbOrderItem> orderItemList = new ArrayList<>();
            TbOrderItem orderItem = new TbOrderItem();
            orderItem.setItemId(itemId);
            orderItem.setGoodsId(item.getGoodsId());
            orderItem.setTitle(item.getTitle());
            orderItem.setPrice(item.getPrice());
            orderItem.setNum(num);
            orderItem.setTotalFee(new BigDecimal(item.getPrice().doubleValue() * num));
            orderItem.setPicPath(item.getImage());
            orderItem.setSellerId(sellerId);
            //放到明细表List
            orderItemList.add(orderItem);
            //放到购物车
            cart.setOrderItemList(orderItemList);
            //放到购物车列表
            cartList.add(cart);
        } else {//5.购物车中有这个商家
            List<TbOrderItem> orderItemList = cart.getOrderItemList();
            //5.1判断该商家明细中有无该件单品
            TbOrderItem orderItem = findItemByItemId(orderItemList, itemId);
            if(orderItem == null){//5.1.1 没有
                orderItem = new TbOrderItem();
                orderItem.setItemId(itemId);
                orderItem.setGoodsId(item.getGoodsId());
                orderItem.setTitle(item.getTitle());
                orderItem.setPrice(item.getPrice());
                orderItem.setNum(num);
                orderItem.setTotalFee(new BigDecimal(item.getPrice().doubleValue() * num));
                orderItem.setPicPath(item.getImage());
                orderItem.setSellerId(sellerId);
                //放到明细表List
                orderItemList.add(orderItem);
            } else {//5.1.2 有这件单品
                //修该单品数量与总价格
                orderItem.setNum(orderItem.getNum() + num);
                orderItem.setTotalFee(new BigDecimal(orderItem.getPrice().doubleValue() * orderItem.getNum()));
                if(orderItem.getNum() <= 0){//当该单品数量小于等于0时, 移除明细表
                    orderItemList.remove(orderItem);
                }
                if(orderItemList.size() <= 0){//当明细表小于等于0时,移除该商家购物车
                    cartList.remove(cart);
                }
            }
        }
        return cartList;
    }

    //根据sellerId查询cartList中有无这个商家
    private Cart findCartBySellerId(List<Cart> cartList, String sellerId){
        for (Cart cart : cartList) {
            if(Objects.equals(cart.getSellerId(), sellerId)){
                return cart;
            }
        }
        return null;
    }

    //根据itemId查询orderItemList中有无这个单品
    private TbOrderItem findItemByItemId(List<TbOrderItem> orderItemList, Long itemId){
        for (TbOrderItem orderItem : orderItemList) {
            if(Objects.equals(orderItem.getItemId(), itemId)){
                return orderItem;
            }
        }
        return null;
    }

    //将购物车信息存入redis
    @Override
    public void addCartListIntoRedis(List<Cart> cartList, String username) {
        redisTemplate.boundHashOps("cartList").put(username, cartList);
    }

    //从Redis中获取购物车信息
    @Override
    public List<Cart> getCartListFromRedis(String username) {
        List<Cart> cartList = (List<Cart>)redisTemplate.boundHashOps("cartList").get(username);
        if(cartList == null){
            return new ArrayList<>();
        }
        return cartList;
    }

    //合并cookie与redis数据
    @Override
    public List<Cart> merageCarList(List<Cart> cookieList, List<Cart> redisList) {
        for (Cart cart : cookieList) {
            List<TbOrderItem> orderItemList = cart.getOrderItemList();
            for (TbOrderItem orderItem : orderItemList) {
                redisList = addCart(redisList, orderItem.getItemId(), orderItem.getNum());
            }
        }
        return redisList;
    }
}
