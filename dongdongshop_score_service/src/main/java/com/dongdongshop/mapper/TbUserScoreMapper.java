package com.dongdongshop.mapper;

import com.dongdongshop.pojo.TbUserScore;
import com.dongdongshop.pojo.TbUserScoreExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TbUserScoreMapper {
    int countByExample(TbUserScoreExample example);

    int deleteByExample(TbUserScoreExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbUserScore record);

    int insertSelective(TbUserScore record);

    List<TbUserScore> selectByExample(TbUserScoreExample example);

    TbUserScore selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TbUserScore record, @Param("example") TbUserScoreExample example);

    int updateByExample(@Param("record") TbUserScore record, @Param("example") TbUserScoreExample example);

    int updateByPrimaryKeySelective(TbUserScore record);

    int updateByPrimaryKey(TbUserScore record);
}