package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbUserScoreMapper;
import com.dongdongshop.pojo.TbUserScore;
import com.dongdongshop.service.UserScoreService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class UserScoreServiceImpl implements UserScoreService {

    @Resource
    private TbUserScoreMapper userScoreMapper;

    @Override
    public void addUserScore(TbUserScore userScore) {
        userScoreMapper.insertSelective(userScore);
    }
}
