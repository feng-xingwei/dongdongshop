package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbUserMapper;
import com.dongdongshop.pojo.TbUser;
import com.dongdongshop.pojo.TbUserExample;
import com.dongdongshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class UserServiceImpl implements UserService {

    @Autowired
    private TbUserMapper userMapper;

    @Override
    public TbUser selectUserById(String username) {
        TbUser tbUser = userMapper.selectUserById(username);
        return tbUser;
    }

    @Override
    public boolean register(TbUser user) {
        int i = userMapper.insertSelective(user);
        return i > 0;
    }

    //查询邮箱
    @Override
    public String getUserMail(String userName) {
        TbUserExample tbUserExample = new TbUserExample();
        TbUserExample.Criteria criteria = tbUserExample.createCriteria();
        criteria.andUsernameEqualTo(userName);
        List<TbUser> tbUsers = userMapper.selectByExample(tbUserExample);
        TbUser user = tbUsers.get(0);
        return user.getEmail();
    }

    //修改密码
    @Override
    public void rePassword(TbUser user) {

        TbUserExample tbUserExample = new TbUserExample();
        TbUserExample.Criteria criteria = tbUserExample.createCriteria();
        criteria.andUsernameEqualTo(user.getUsername());
        userMapper.updateByExampleSelective(user, tbUserExample);
    }
}
