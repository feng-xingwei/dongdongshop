package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbContentCategoryMapper;
import com.dongdongshop.mapper.TbContentMapper;
import com.dongdongshop.pojo.TbContent;
import com.dongdongshop.pojo.TbContentCategory;
import com.dongdongshop.pojo.TbContentCategoryExample;
import com.dongdongshop.pojo.TbContentExample;
import com.dongdongshop.service.ContentService;
import com.dongdongshop.service.IndexContentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

@Service
@com.alibaba.dubbo.config.annotation.Service


public class IndexContentServiceImpl implements IndexContentService {

    @Resource
    private TbContentMapper contentMapper;


    //广告图

    @Override
    public List<TbContent> getContentList(Long category) {
        TbContentExample tbContentExample = new TbContentExample();
        TbContentExample.Criteria criteria = tbContentExample.createCriteria();
        criteria.andCategoryIdEqualTo(category);
        return contentMapper.selectByExample(tbContentExample);
    }


    @Override
    public List<TbContent> selectAll() {
        List<TbContent> contents = contentMapper.selectAll(1);
        return contents;
    }
}
