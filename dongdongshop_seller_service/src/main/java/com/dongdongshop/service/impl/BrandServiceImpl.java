package com.dongdongshop.service.impl;

import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.mapper.TbBrandMapper;
import com.dongdongshop.pojo.TbBrand;
import com.dongdongshop.data.Result;
import com.dongdongshop.service.BrandService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class BrandServiceImpl implements BrandService {

    @Resource
    private TbBrandMapper brandMapper;

    @Override
    public Result getBrandList() {
        List<TbBrand> tbBrands = null;
        try {
            tbBrands = brandMapper.selectByExample(null);
        }catch (Exception e){
            e.printStackTrace();
            return Result.build(ConstantEnum.ERROR_RETRIEVE);
        }
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(tbBrands);
    }

    @Override
    public Result addBrand(TbBrand tbBrand) {
        try {
            brandMapper.insertSelective(tbBrand);
        }catch (Exception e){
            e.printStackTrace();
            return Result.build(ConstantEnum.ERROR_CREATE);
        }
        return Result.build(ConstantEnum.SUCCESS_CREATE);
    }

    @Override
    public Result selectBrandById(Integer id) {
        TbBrand brand = null;
        try {
            brand = brandMapper.selectByPrimaryKey(id.longValue());
        }catch (Exception e){
            e.printStackTrace();
            return Result.build(ConstantEnum.ERROR_BACK);
        }
        return Result.build(ConstantEnum.SUCCESS_BACK).setResult(brand);
    }

    @Override
    public Result updateBrand(TbBrand tbBrand) {
        try {
            brandMapper.updateByPrimaryKeySelective(tbBrand);
        }catch (Exception e){
            e.printStackTrace();
            return Result.build(ConstantEnum.ERROR_UPDATE);
        }
        return Result.build(ConstantEnum.SUCCESS_UPDATE);
    }

    @Override
    public Result deleteBatch(Integer[] ids) {
        try {
            brandMapper.deleteByIds(ids);
        }catch (Exception e){
            e.printStackTrace();
            return Result.build(ConstantEnum.ERROR_DELETE);
        }
        return Result.build(ConstantEnum.SUCCESS_DELETE);
    }
}
