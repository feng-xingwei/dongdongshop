package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbContentCategoryMapper;
import com.dongdongshop.mapper.TbContentMapper;
import com.dongdongshop.pojo.TbContent;
import com.dongdongshop.pojo.TbContentCategory;
import com.dongdongshop.pojo.TbContentCategoryExample;
import com.dongdongshop.pojo.TbContentExample;
import com.dongdongshop.service.ContentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

@Service
@com.alibaba.dubbo.config.annotation.Service


public class ContentServiceImpl implements ContentService {

    @Resource
    private TbContentMapper contentMapper;

    @Resource
    private TbContentCategoryMapper contentCategoryMapper;

    //广告图分类

    @Override
    public List<TbContentCategory> getContentCategoryList(String name) {
        TbContentCategoryExample tbContentCategoryExample = new TbContentCategoryExample();
        TbContentCategoryExample.Criteria criteria = tbContentCategoryExample.createCriteria();
        if (name != null && !"".equals(name)){
            criteria.andNameLike("%" + name + "%");
        }
        List<TbContentCategory> contentCategoryList = contentCategoryMapper.selectByExample(tbContentCategoryExample);
        return contentCategoryList;
    }

    @Override
    public boolean addContentCategory(TbContentCategory category) {
        int i = contentCategoryMapper.insertSelective(category);
        return i > 0;
    }

    @Override
    public TbContentCategory getContentCategoryBack(Long id) {
        TbContentCategory category = contentCategoryMapper.selectByPrimaryKey(id);
        return category;
    }

    @Override
    public boolean updateContentCategory(TbContentCategory category) {
        int i = contentCategoryMapper.updateByPrimaryKeySelective(category);
        return i > 0;
    }

    @Override
    public boolean deleteContentCategoryBatch(Long[] ids) {

        Consumer con = (id) -> {
            TbContentCategoryExample tbContentCategoryExample = new TbContentCategoryExample();
            TbContentCategoryExample.Criteria criteria = tbContentCategoryExample.createCriteria();
            criteria.andIdEqualTo((Long) id);
            contentCategoryMapper.deleteByExample(tbContentCategoryExample);
        };
        Arrays.stream(ids).forEach(con);

        return true;
    }

    //广告图

    @Override
    public List<TbContent> getContentList() {
        return contentMapper.selectAll();
    }

    @Override
    public List<TbContent> getContentList(Long category) {
        TbContentExample tbContentExample = new TbContentExample();
        TbContentExample.Criteria criteria = tbContentExample.createCriteria();
        criteria.andCategoryIdEqualTo(category);
        return contentMapper.selectByExample(tbContentExample);
    }

    @Override
    public boolean addContent(TbContent content) {
        int i = contentMapper.insertSelective(content);
        return i > 0;
    }

    @Override
    public TbContent getContentBack(Long id) {
        TbContent content = contentMapper.selectByPrimaryKey(id);
        return content;
    }

    @Override
    public boolean updateContent(TbContent content) {
        int i = contentMapper.updateByPrimaryKeySelective(content);
        return i > 0;
    }

    @Override
    public boolean deleteContentBatch(Long[] ids) {

        Consumer con = (id) -> {
            TbContentExample tbContentExample = new TbContentExample();
            TbContentExample.Criteria criteria = tbContentExample.createCriteria();
            criteria.andIdEqualTo((Long) id);
            contentMapper.deleteByExample(tbContentExample);
        };
        Arrays.stream(ids).forEach(con);

        return true;
    }
}
