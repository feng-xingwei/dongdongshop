package com.dongdongshop.service.impl;

import com.dongdongshop.data.Page;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.mapper.TbItemCatMapper;
import com.dongdongshop.mapper.TbItemMapper;
import com.dongdongshop.pojo.TbItem;
import com.dongdongshop.pojo.TbItemCat;
import com.dongdongshop.pojo.TbItemCatExample;
import com.dongdongshop.pojo.TbItemExample;
import com.dongdongshop.service.ItemService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class ItemServiceImpl implements ItemService {

    @Resource
    private TbItemCatMapper itemCatMapper;

    @Resource
    private TbItemMapper itemMapper;

    @Override
    public Page<List<TbItemCat>> getItemList(Long id, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        TbItemCatExample tbItemCatExample = new TbItemCatExample();
        TbItemCatExample.Criteria criteria = tbItemCatExample.createCriteria();
        criteria.andParentIdEqualTo(id);
        List<TbItemCat> itemCats = itemCatMapper.selectByExample(tbItemCatExample);
        PageInfo<TbItemCat> pageInfo = new PageInfo<>(itemCats);
        return new Page(pageNum, pageSize, pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public Result getItem(Long id) {
        List<TbItemCat> list = getItem2(id, new ArrayList<>());
        Collections.reverse(list);
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(list);
    }

    public List<TbItemCat> getItem2(Long id, List<TbItemCat> list){
        if(id == 0){
            list.add(new TbItemCat(0L, "顶级分类列表"));
        }else{
            TbItemCat tbItemCat = itemCatMapper.selectByPrimaryKey(id);
            list.add(tbItemCat);
            getItem2(tbItemCat.getParentId(), list);
        }
        return list;
    }

    @Override
    public Result addItem(TbItemCat itemCat) {
        try {
            int i = itemCatMapper.insertSelective(itemCat);
            if (i <= 0){
                return Result.build(ConstantEnum.ERROR_CREATE);
            }
        }catch (Exception e){
            e.printStackTrace();
            return Result.build(ConstantEnum.ERROR_CREATE);
        }
        return Result.build(ConstantEnum.SUCCESS_CREATE);
    }

    @Override
    public Result toUpdate(Long id) {
        TbItemCat tbItemCat = itemCatMapper.selectByPrimaryKey(id);
        return Result.build(ConstantEnum.SUCCESS_BACK).setResult(tbItemCat);
    }

    @Override
    public Result updateItem(TbItemCat itemCat) {
        try {
            int i = itemCatMapper.updateByPrimaryKey(itemCat);
            if (i <= 0){
                return Result.build(ConstantEnum.ERROR_UPDATE);
            }
        }catch (Exception e){
            e.printStackTrace();
            return Result.build(ConstantEnum.ERROR_UPDATE);
        }
        return Result.build(ConstantEnum.SUCCESS_UPDATE);
    }

    @Override
    public Result deleteBatch(Long[] ids) {
        Arrays.stream(ids).forEach((id) -> {
            TbItemCatExample tbItemCatExample = new TbItemCatExample();
            TbItemCatExample.Criteria criteria = tbItemCatExample.createCriteria();
            criteria.andIdEqualTo((Long) id);
            itemCatMapper.deleteByExample(tbItemCatExample);
        });
        return Result.build(ConstantEnum.SUCCESS_DELETE);
    }

    @Override
    public List<TbItem> getItemsByGoodId(Long goodId) {
        TbItemExample tbItemExample = new TbItemExample();
        TbItemExample.Criteria criteria = tbItemExample.createCriteria();
        criteria.andGoodsIdEqualTo(goodId);
        criteria.andStatusEqualTo("1");
        criteria.andStockCountGreaterThan(0);
        tbItemExample.setOrderByClause(" is_default desc ");
        List<TbItem> tbItems = itemMapper.selectByExample(tbItemExample);
        return tbItems;
    }

    @Override
    public TbItem getItemByItemId(Long itemId) {
        return itemMapper.selectByPrimaryKey(itemId);
    }
}
