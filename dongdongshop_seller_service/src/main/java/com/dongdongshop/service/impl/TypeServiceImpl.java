package com.dongdongshop.service.impl;

import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.mapper.TbTypeTemplateMapper;
import com.dongdongshop.pojo.TbTypeTemplate;
import com.dongdongshop.pojo.TbTypeTemplateExample;
import com.dongdongshop.data.Result;
import com.dongdongshop.service.TypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class TypeServiceImpl implements TypeService {

    @Resource
    private TbTypeTemplateMapper typeTemplateMapper;

    @Override
    public Result getTypeList(String name) {
        TbTypeTemplateExample tbTypeTemplateExample = new TbTypeTemplateExample();
        TbTypeTemplateExample.Criteria criteria = tbTypeTemplateExample.createCriteria();
        if(name != null && !"".equals(name)){
            criteria.andNameLike("%" + name + "%");
        }
        List<TbTypeTemplate> typeList = typeTemplateMapper.selectByExample(tbTypeTemplateExample);
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(typeList);
    }

    @Override
    public Result addType(TbTypeTemplate typeTemplate) {
        try {
            int i = typeTemplateMapper.insertSelective(typeTemplate);
            if (i <= 0){
                return Result.build(ConstantEnum.ERROR_CREATE);
            }
        }catch (Exception e){
            e.printStackTrace();
            return Result.build(ConstantEnum.ERROR_CREATE);
        }
        return Result.build(ConstantEnum.SUCCESS_CREATE);
    }

    @Override
    public Result getTypeById(Integer id) {
        TbTypeTemplate typeTemplate = null;
        try {
            typeTemplate = typeTemplateMapper.selectByPrimaryKey(id.longValue());
        }catch (Exception e){
            e.printStackTrace();
            return Result.build(ConstantEnum.ERROR_BACK);
        }
        return Result.build(ConstantEnum.SUCCESS_BACK).setResult(typeTemplate);
    }

    @Override
    public Result updateType(TbTypeTemplate typeTemplate) {
        try {
            int i = typeTemplateMapper.updateByPrimaryKeySelective(typeTemplate);
            if (i <= 0){
                return Result.build(ConstantEnum.ERROR_UPDATE);
            }
        }catch (Exception e){
            e.printStackTrace();
            return Result.build((ConstantEnum.ERROR_UPDATE));
        }
        return Result.build(ConstantEnum.SUCCESS_UPDATE);
    }

    @Override
    public Result deleteBatch(Long[] ids) {

        Consumer consumer = (id) -> {
            TbTypeTemplateExample tbTypeTemplateExample = new TbTypeTemplateExample();
            TbTypeTemplateExample.Criteria criteria = tbTypeTemplateExample.createCriteria();
            criteria.andIdEqualTo(((Long) id));
            typeTemplateMapper.deleteByExample(tbTypeTemplateExample);
        };
        Arrays.stream(ids).forEach(consumer);

        return Result.build(ConstantEnum.SUCCESS_DELETE);
    }
}
