package com.dongdongshop.service.impl;

import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.mapper.TbSellerMapper;
import com.dongdongshop.pojo.TbSeller;
import com.dongdongshop.pojo.TbSellerExample;
import com.dongdongshop.service.SellerService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class SellerServiceImpl implements SellerService {

    @Resource
    private TbSellerMapper sellerMapper;


    //展示商家列表
    @Override
    public Result getSellerList(TbSeller seller) {
        List<TbSeller> sellerList = sellerMapper.selectSellerList(seller);
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(sellerList);
    }

    //注册
    @Override
    public Result addSeller(TbSeller seller) {
        try {
            int i = sellerMapper.insertSelective(seller);
            if (i <= 0){
                return Result.build(ConstantEnum.ERROR_CREATE);
            }
        }catch (Exception e){
            e.printStackTrace();
            return Result.build(ConstantEnum.ERROR_CREATE);
        }
        return Result.build(ConstantEnum.SUCCESS_CREATE);
    }

    //登录
    @Override
    public Result getSellerBack(String id) {
        TbSellerExample tbSellerExample = new TbSellerExample();
        TbSellerExample.Criteria criteria = tbSellerExample.createCriteria();
        criteria.andSellerIdEqualTo(id);
        List<TbSeller> sellerList = sellerMapper.selectByExample(tbSellerExample);
        System.out.println(id);
        System.out.println(sellerList.get(0));
        return Result.build(ConstantEnum.SUCCESS_BACK).setResult(sellerList.get(0));
    }

    //审核
    @Override
    public Result updateSellerStatus(String sellerId, String status) {
        try {
            int i = sellerMapper.updateSellerStatus(sellerId, status);
            if (i <= 0){
                return Result.build(ConstantEnum.ERROR_UPDATE);
            }
        }catch (Exception e){
            e.printStackTrace();
            return Result.build(ConstantEnum.ERROR_UPDATE);
        }
        return Result.build(ConstantEnum.SUCCESS_UPDATE);
    }

    @Override
    public TbSeller selectSellerById(String username) {
        return sellerMapper.selectSellerById(username);
    }
}
