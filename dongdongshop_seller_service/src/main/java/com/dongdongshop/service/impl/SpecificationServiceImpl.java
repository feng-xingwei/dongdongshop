package com.dongdongshop.service.impl;

import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.mapper.TbSpecificationMapper;
import com.dongdongshop.mapper.TbSpecificationOptionMapper;
import com.dongdongshop.pojo.TbSpecification;
import com.dongdongshop.pojo.TbSpecificationExample;
import com.dongdongshop.pojo.TbSpecificationOption;
import com.dongdongshop.pojo.TbSpecificationOptionExample;
import com.dongdongshop.data.Result;
import com.dongdongshop.service.SpecificationService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class SpecificationServiceImpl implements SpecificationService {

    @Resource
    private TbSpecificationMapper specificationMapper;

    @Resource
    private TbSpecificationOptionMapper specificationOptionMapper;

    @Override
    public Result getSpecificationList(String specName) {
        List<TbSpecification> tbSpecificationList = null;
        try {
            tbSpecificationList = specificationMapper.getSpecificationList(specName);
        }catch (Exception e){
            e.printStackTrace();
            return Result.build(ConstantEnum.ERROR_RETRIEVE);
        }
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(tbSpecificationList);
    }

    @Override
    public Result addSpecification(TbSpecification specification, List<TbSpecificationOption> tbSpecificationOptions) {
        specificationMapper.insertSelective(specification);
        specificationOptionMapper.insertBySpecificationId(specification.getId(), tbSpecificationOptions);
        return Result.build(ConstantEnum.SUCCESS_CREATE);
    }

    @Override
    public Result getSpecificationById(Integer id) {
        List<TbSpecification> specificationList = specificationMapper.selectSpecificationListById(id.longValue());
        return Result.build(ConstantEnum.SUCCESS_BACK).setResult(specificationList);
    }

    @Override
    public Result updateSpecification(TbSpecification specification, List<TbSpecificationOption> tbSpecificationOptions) {
        specificationMapper.updateByPrimaryKeySelective(specification);

        TbSpecificationOptionExample tbSpecificationOptionExample = new TbSpecificationOptionExample();
        TbSpecificationOptionExample.Criteria criteria = tbSpecificationOptionExample.createCriteria();
        criteria.andSpecIdEqualTo(specification.getId());
        int i = specificationOptionMapper.deleteByExample(tbSpecificationOptionExample);

        specificationOptionMapper.insertBySpecificationId(specification.getId(),tbSpecificationOptions);

        return Result.build(ConstantEnum.SUCCESS_UPDATE);
    }

    @Override
    public Result deleteBatch(Integer[] ids) {
        specificationMapper.deleteByIds(ids);

        for(int i = 0; i < ids.length; i++){
            TbSpecificationOptionExample tbSpecificationOptionExample = new TbSpecificationOptionExample();
            TbSpecificationOptionExample.Criteria criteria = tbSpecificationOptionExample.createCriteria();
            criteria.andSpecIdEqualTo(ids[i].longValue());
            specificationOptionMapper.deleteByExample(tbSpecificationOptionExample);
        }

        return Result.build(ConstantEnum.SUCCESS_DELETE);
    }

    @Override
    public Result getSpecNameById(Long specId) {
        TbSpecificationOptionExample tbSpecificationOptionExample = new TbSpecificationOptionExample();
        TbSpecificationOptionExample.Criteria criteria = tbSpecificationOptionExample.createCriteria();
        criteria.andSpecIdEqualTo(specId);
        List<TbSpecificationOption> tbSpecificationOptions = specificationOptionMapper.selectByExample(tbSpecificationOptionExample);
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(tbSpecificationOptions);
    }

    @Override
    public Result getSpecification(Long id) {
        TbSpecificationExample tbSpecificationExample = new TbSpecificationExample();
        TbSpecificationExample.Criteria criteria = tbSpecificationExample.createCriteria();
        criteria.andIdEqualTo(id);
        List<TbSpecification> specificationList = specificationMapper.selectByExample(tbSpecificationExample);
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(specificationList.get(0));
    }
}
