package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Page;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.TbItemCat;
import com.dongdongshop.service.ItemService;
import com.dongdongshop.service.TypeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("itemController")
public class ItemController {

    @Reference
    private ItemService itemService;

    @Reference
    private TypeService typeService;

    @RequestMapping("toItem")
    public String toItem(Model model){
        Result typeList = typeService.getTypeList("");
        model.addAttribute("types", typeList.getResult());
        return "admin/item_cat";
    }

    @RequestMapping("getItemList")
    @ResponseBody
    public Result getItemList(@RequestParam(defaultValue = "0") Long id, @RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "99") Integer pageSize){
        Page<List<TbItemCat>> page = itemService.getItemList(id, pageNum, pageSize);
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(page);
    }

    @RequestMapping("getItem")
    @ResponseBody
    public Result getItem(Long id){
        Result result = itemService.getItem(id);
        return result;
    }

    @RequestMapping("addItem")
    @ResponseBody
    public Result addItem(TbItemCat itemCat){
        Result result = itemService.addItem(itemCat);
        return result;
    }

    @RequestMapping("toUpdate/{id}")
    @ResponseBody
    public Result toUpdate(@PathVariable("id")Long id){
        Result result = itemService.toUpdate(id);
        return result;
    }

    @RequestMapping("updateItem")
    @ResponseBody
    public Result updateItem(TbItemCat itemCat){
        Result result = itemService.updateItem(itemCat);
        return result;
    }

    @RequestMapping("deleteBatch")
    @ResponseBody
    public Result deleteBatch(Long[] ids){
        Result result = itemService.deleteBatch(ids);
        return result;
    }
}
