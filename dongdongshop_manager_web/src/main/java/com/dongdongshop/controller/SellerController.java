package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.pojo.TbSeller;
import com.dongdongshop.service.SellerService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("sellerController")
public class SellerController {

    @Reference
    private SellerService sellerService;

    @RequestMapping("toSeller")
    public String toSeller(){
        return "admin/seller";
    }

    @RequestMapping("getSellerList")
    @ResponseBody
    public Result getSellerList(TbSeller seller){
        Result result = sellerService.getSellerList(seller);
        return result;
    }

    @RequestMapping("getSellerBack/{id}")
    @ResponseBody
    public Result getSellerBack(@PathVariable("id") String id){
        Result result = sellerService.getSellerBack(id);
        return result;
    }

    @RequestMapping("updateSellerStatus")
    @ResponseBody
    public Result updateSellerStatus(String sellerId, String status){
        Result result = sellerService.updateSellerStatus(sellerId, status);
        return result;
    }
}
