package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.TbBrand;
import com.dongdongshop.data.Result;
import com.dongdongshop.service.BrandService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.nio.charset.StandardCharsets;

@Controller
@RequestMapping("brandController")
public class BrandController {

    @Reference
    private BrandService brandService;

    @RequestMapping("toBrand")
    public String toBrand(){
        return "admin/brand";
    }

    @RequestMapping("getBrandList")
    @ResponseBody
    public Result getBrandList(){
        Result result = brandService.getBrandList();
        return result;
    }

    @RequestMapping("addBrand")
    @ResponseBody
    public Result addBrand(TbBrand tbBrand){
        if("".equals(tbBrand.getName()) || tbBrand.getFirstChar() == null){
            return Result.build(ConstantEnum.EXCEPTION_ARGS);
        }
        if(tbBrand.getFirstChar().getBytes(StandardCharsets.UTF_8).length != 1){
            return Result.build(ConstantEnum.EXCEPTION_FIRST_CHAR);
        }
        Result result = brandService.addBrand(tbBrand);
        return result;
    }

    @RequestMapping("toUpdate/{id}")
    @ResponseBody
    public Result toUpdate(@PathVariable("id")Integer id){
        Result result = brandService.selectBrandById(id);
        return result;
    }

    @RequestMapping("updateBrand")
    @ResponseBody
    public Result updateBrand(TbBrand tbBrand){
        if("".equals(tbBrand.getName()) || tbBrand.getFirstChar() == null){
            return Result.build(ConstantEnum.EXCEPTION_ARGS);
        }
        if(tbBrand.getFirstChar().getBytes(StandardCharsets.UTF_8).length != 1){
            return Result.build(ConstantEnum.EXCEPTION_FIRST_CHAR);
        }
        Result result = brandService.updateBrand(tbBrand);
        return result;
    }

    @RequestMapping("deleteBatch")
    @ResponseBody
    public Result deleteBatch(Integer[] ids){
        Result result = brandService.deleteBatch(ids);
        return result;
    }

}
