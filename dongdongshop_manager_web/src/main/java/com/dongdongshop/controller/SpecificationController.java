package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.dongdongshop.pojo.TbSpecification;
import com.dongdongshop.pojo.TbSpecificationOption;
import com.dongdongshop.data.Result;
import com.dongdongshop.service.SpecificationService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("specificationController")
public class SpecificationController {

    @Reference
    private SpecificationService specificationService;

    @RequestMapping("toSpecification")
    public String toSpecification(){
        return "admin/specification";
    }

    @RequestMapping("getSpecificationList")
    @ResponseBody
    public Result getSpecificationList(String specName){
        return specificationService.getSpecificationList(specName);
    }

    @RequestMapping("addSpecification")
    @ResponseBody
    public Result addSpecification(String specName, String specOptionListStr){
        TbSpecification specification = new TbSpecification();
        specification.setSpecName(specName);

        List<TbSpecificationOption> tbSpecificationOptions = JSONObject.parseArray(specOptionListStr, TbSpecificationOption.class);
        return specificationService.addSpecification(specification, tbSpecificationOptions);
    }

    @RequestMapping("toUpdate/{id}")
    @ResponseBody
    public Result toUpdate(@PathVariable("id")Integer id){
        Result result = specificationService.getSpecificationById(id);
        return result;
    }

    @RequestMapping("updateSpecification")
    @ResponseBody
    public Result updateSpecification(Long id, String specName, String specOptionListStr){
        TbSpecification specification = new TbSpecification();
        specification.setId(id);
        specification.setSpecName(specName);

        List<TbSpecificationOption> tbSpecificationOptions = JSONObject.parseArray(specOptionListStr, TbSpecificationOption.class);
        return specificationService.updateSpecification(specification, tbSpecificationOptions);
    }

    @RequestMapping("deleteBatch")
    @ResponseBody
    public Result deleteBatch(Integer[] ids){
        Result result = specificationService.deleteBatch(ids);
        return result;
    }
}
