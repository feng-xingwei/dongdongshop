package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.TbContent;
import com.dongdongshop.pojo.TbContentCategory;
import com.dongdongshop.service.ContentService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("contentController")
public class ContentController {

    @Reference
    private ContentService contentService;

    @RequestMapping("toContent")
    public String toContent(Model model) {
        List<TbContentCategory> contentCategoryList = contentService.getContentCategoryList("");
        model.addAttribute("categories", contentCategoryList);
        return "admin/content";
    }

    @RequestMapping("toContentCategory")
    public String toContentCategory(){
        return "admin/content_category";
    }

    @RequestMapping("getContentCategoryList")
    @ResponseBody
    public Result getContentCategoryList(String name){
        List<TbContentCategory> contentCategoryList = contentService.getContentCategoryList(name);
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(contentCategoryList);
    }

    @RequestMapping("addContentCategory")
    @ResponseBody
    public Result addContentCategory(TbContentCategory category){
        boolean result = contentService.addContentCategory(category);
        if (!result){
            return Result.build(ConstantEnum.ERROR_CREATE);
        } else {
            return Result.build(ConstantEnum.SUCCESS_CREATE);
        }
    }

    @RequestMapping("getContentCategoryBack/{id}")
    @ResponseBody
    public Result getContentCategoryBack(@PathVariable("id") Long id){
        TbContentCategory category = contentService.getContentCategoryBack(id);
        return Result.build(ConstantEnum.SUCCESS_BACK).setResult(category);
    }

    @RequestMapping("updateContentCategory")
    @ResponseBody
    public Result updateContentCategory(TbContentCategory category){
        boolean result = contentService.updateContentCategory(category);
        if(!result){
            return Result.build(ConstantEnum.ERROR_UPDATE);
        }else {
            return Result.build(ConstantEnum.SUCCESS_UPDATE);
        }
    }

    @RequestMapping("deleteContentCategoryBatch")
    @ResponseBody
    public Result deleteContentCategoryBatch(Long[] ids){
        boolean result = contentService.deleteContentCategoryBatch(ids);
        return Result.build(ConstantEnum.SUCCESS_DELETE);
    }

    @RequestMapping("getContentList")
    @ResponseBody
    public Result getContentList(){
        List<TbContent> contents = contentService.getContentList();
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(contents);
    }

    @RequestMapping("addContent")
    @ResponseBody
    public Result addContent(TbContent content){
        boolean result = contentService.addContent(content);
        if(!result){
            return Result.build(ConstantEnum.ERROR_CREATE);
        }else {
            return Result.build(ConstantEnum.SUCCESS_CREATE);
        }
    }

    @RequestMapping("getContentBack/{id}")
    @ResponseBody
    public Result getContentBack(@PathVariable("id")Long id){
        TbContent content = contentService.getContentBack(id);
        return Result.build(ConstantEnum.SUCCESS_BACK).setResult(content);
    }

    @RequestMapping("updateContent")
    @ResponseBody
    public Result updateContent(TbContent content){
        boolean result = contentService.updateContent(content);
        if(!result){
            return Result.build(ConstantEnum.ERROR_UPDATE);
        }else {
            return Result.build(ConstantEnum.SUCCESS_UPDATE);
        }
    }

    @RequestMapping("deleteContentBatch")
    @ResponseBody
    public Result deleteBatch(Long[] ids){
        boolean result = contentService.deleteContentBatch(ids);
        return Result.build(ConstantEnum.SUCCESS_DELETE);
    }

}
