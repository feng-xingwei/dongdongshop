package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.pojo.TbItemCat;
import com.dongdongshop.pojo.TbTypeTemplate;
import com.dongdongshop.data.Result;
import com.dongdongshop.service.BrandService;
import com.dongdongshop.service.SpecificationService;
import com.dongdongshop.service.TypeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("typeController")
public class TypeController {

    @Reference
    private TypeService typeService;

    @Reference
    private BrandService brandService;

    @Reference
    private SpecificationService specificationService;

    @RequestMapping("toType")
    public String toType(Model model){
        Result brandResult = brandService.getBrandList();
        model.addAttribute("brandList", brandResult.getResult());
        Result specificationResult = specificationService.getSpecificationList("");
        model.addAttribute("specList", specificationResult.getResult());
        return "admin/type_template";
    }

    @RequestMapping("getTypeList")
    @ResponseBody
    public Result getTypeList(String name){
        Result result = typeService.getTypeList(name);
        return result;
    }

    @RequestMapping("addType")
    @ResponseBody
    public Result addType(TbTypeTemplate typeTemplate){
        Result result = typeService.addType(typeTemplate);
        return result;
    }

    @RequestMapping("toUpdate/{id}")
    @ResponseBody
    public Result toUpdate(@PathVariable("id")Integer id){
        Result result = typeService.getTypeById(id);
        return result;
    }

    @RequestMapping("updateType")
    @ResponseBody
    public Result updateType(TbTypeTemplate typeTemplate){
        Result result = typeService.updateType(typeTemplate);
        return result;
    }

    @RequestMapping("deleteBatch")
    @ResponseBody
    public Result deleteBatch(Long[] ids){
        Result result = typeService.deleteBatch(ids);
        return result;
    }

}
