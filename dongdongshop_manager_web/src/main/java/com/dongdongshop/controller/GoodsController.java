package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.pojo.TbGoods;
import com.dongdongshop.pojo.TbSeller;
import com.dongdongshop.service.GoodService;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("goodsController")
public class GoodsController {

    @Reference
    private GoodService goodService;

    @RequestMapping("toGoods")
    public String toGoods(){
        return "admin/goods";
    }

    @RequestMapping("getGoodList")
    @ResponseBody
    public Result getGoodList(TbGoods goods){
        Result result = goodService.getAllGoodList(goods);
        return result;
    }

    //批量审核
    @RequestMapping("updateBatch")
    @ResponseBody
    public Result updateBatch(String[] ids, String status){
        Result result = goodService.updateBatch(ids, status);
        return result;
    }

    //批量删除
    @RequestMapping("deleteBatch")
    @ResponseBody
    public Result deleteBatch(String[] ids){
        Result result = goodService.deleteBatch(ids);
        return result;
    }
}
