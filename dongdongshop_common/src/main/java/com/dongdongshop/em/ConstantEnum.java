package com.dongdongshop.em;

public enum ConstantEnum {

    SUCCESS(10000, "操作成功"),
    SUCCESS_RETRIEVE(10001, "获取列表成功"),
    SUCCESS_CREATE(10002, "增加成功"),
    SUCCESS_BACK(10003, "回显成功"),
    SUCCESS_UPDATE(10004, "修改成功"),
    SUCCESS_DELETE(10005, "删除成功"),

    EXCEPTION_ARGS(30002, "参数为空"),
    EXCEPTION_FIRST_CHAR(30003, "首字母非法"),


    ERROR(20000, "操作失败"),
    ERROR_RETRIEVE(20001, "获取列表失败"),
    ERROR_CREATE(20002, "增加失败"),
    ERROR_BACK(20003, "回显失败"),
    ERROR_UPDATE(20004, "修改失败"),
    ERROR_DELETE(20005, "删除失败"),

    NO_AUTH(30001, "认证失败");

    private Integer code;
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    ConstantEnum() {
    }

    ConstantEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
