package com.dongdongshop.data;

import com.dongdongshop.em.ConstantEnum;

import java.io.Serializable;

public class Result<T> implements Serializable {

    private Integer statusCode;

    private String message;

    private T result;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResult() {
        return result;
    }

    public Result setResult(T result) {
        this.result = result;
        return this;
    }

    public static Result build(ConstantEnum e){
        Result result = new Result();
        result.setStatusCode(e.getCode());
        result.setMessage(e.getMessage());
        return result;
    }

    public Result(Integer statusCode, String message, T result) {
        this.statusCode = statusCode;
        this.message = message;
        this.result = result;
    }

    public Result(Integer statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    public Result() {
    }
}
