package com.dongdongshop.mq;

import com.dongdongshop.mapper.TbSeckillGoodsMapper;
import com.dongdongshop.mapper.TbSeckillOrderMapper;
import com.dongdongshop.pojo.TbSeckillGoods;
import com.dongdongshop.pojo.TbSeckillOrder;
import com.dongdongshop.util.IdWorker;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.listener.Topic;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

@Component
@RocketMQMessageListener(topic = "seckill-goods", consumerGroup = "seckill-consumer")
public class SeckillGoodsMQListener implements RocketMQListener<String> {

    @Resource
    private TbSeckillGoodsMapper seckillGoodsMapper;

    @Resource
    private TbSeckillOrderMapper seckillOrderMapper;

    @Override
    public void onMessage(String s) {
        String[] split = s.split(",");
        long seckillId = Long.parseLong(split[1]);
        TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(seckillId);
        //生成订单
        IdWorker idWorker = new IdWorker();
        TbSeckillOrder seckillOrder = new TbSeckillOrder();
        seckillOrder.setId(idWorker.nextId());
        seckillOrder.setCreateTime(new Date());
        seckillOrder.setMoney(seckillGoods.getCostPrice());
        seckillOrder.setSeckillId(seckillId);
        seckillOrder.setSellerId(seckillGoods.getSellerId());
        seckillOrder.setStatus("0");
        seckillOrder.setUserId(split[0]);
        seckillOrderMapper.insertSelective(seckillOrder);
    }
}
