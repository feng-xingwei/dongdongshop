package com.dongdongshop.service.impl;

import com.dongdongshop.po.Comment;
import com.dongdongshop.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void addComment(Comment comment) {
        mongoTemplate.insert(comment);
    }

    @Override
    public List<Comment> getCommentList(Long goodsId, Integer star) {
        Query query = new Query();
        query.addCriteria(Criteria.where("goodsId").is(goodsId));
        if(star != 0){
            query.addCriteria(Criteria.where("star").is(star));
        }
        List<Comment> comments = mongoTemplate.find(query, Comment.class);
        return comments;
    }
}
