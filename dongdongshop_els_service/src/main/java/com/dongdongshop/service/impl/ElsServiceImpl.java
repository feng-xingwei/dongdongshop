package com.dongdongshop.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.dongdongshop.po.NormalResult;
import com.dongdongshop.po.SearchResult;
import com.dongdongshop.service.ElsService;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class ElsServiceImpl implements ElsService {

    @Autowired
    private RestHighLevelClient client;

    @Override
    public List<SearchResult> getSearchResult(String input) {
        // 1.构建SearchRequest请求对象,指定索引库
        SearchRequest searchRequest = new SearchRequest("dongdongshop");
        // 2.构建SearchSourceBuilder查询对象
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // 3.构建查询方式
        MatchQueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("goods_name", input);
        // 4.将QueryBuilder对象设置到SearchSourceBuilder中
        searchSourceBuilder.query(matchQueryBuilder);
        // 5.构建HighlightBuilder对象
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        // 5.1. 设置要高亮的字段
        highlightBuilder.field("goods_name");
        // 5.2. 设置高亮格式
        highlightBuilder.preTags("<font color='red'>");
        highlightBuilder.postTags("</font>");
        // 6. 将高亮对象highlightBuilder设置到sourceBuilder中
        searchSourceBuilder.highlighter(highlightBuilder);
        // 7.将SearchSourceBuilder查询对象封装到请求对象SearchRequest中
        searchRequest.source(searchSourceBuilder);
        // 8.调用方法进行数据通信
        List<SearchResult> searchResultList = new ArrayList<>();
        try {
            SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
            // 9.解析输出结果
            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                SearchResult searchResult = new SearchResult();
                String sourceAsString = hit.getSourceAsString();
                System.out.println("普通结果" + sourceAsString);
                NormalResult normalResult = JSONObject.parseObject(sourceAsString, NormalResult.class);
                searchResult.setNormalResult(normalResult);
                // 9.1 获取高亮结果
                Map<String, HighlightField> highlightFields = hit.getHighlightFields();
                // 9.2 解析高亮结果
                HighlightField highlightField = highlightFields.get("goods_name");
                Text[] fragments = highlightField.getFragments();
                // 9.3 遍历高亮结果
                for (Text fragment : fragments) {
                    System.out.println("高亮结果" + fragment.toString());
                    searchResult.setHighlightResult(fragment.toString());
                }
                searchResultList.add(searchResult);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return searchResultList;
    }
}
