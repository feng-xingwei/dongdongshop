package com.dongdongshop.po;

import java.io.Serializable;
import java.util.List;

public class SearchResult implements Serializable {
    private NormalResult normalResult;
    private String highlightResult;

    public NormalResult getNormalResult() {
        return normalResult;
    }

    public void setNormalResult(NormalResult normalResult) {
        this.normalResult = normalResult;
    }

    public String getHighlightResult() {
        return highlightResult;
    }

    public void setHighlightResult(String highlightResult) {
        this.highlightResult = highlightResult;
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "normalResult='" + normalResult + '\'' +
                ", highlightResult='" + highlightResult + '\'' +
                '}';
    }
}
