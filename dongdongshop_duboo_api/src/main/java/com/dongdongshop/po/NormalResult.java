package com.dongdongshop.po;

import java.io.Serializable;

public class NormalResult implements Serializable {
    private Long price;
    private String goods_name;

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }
}
