package com.dongdongshop.po;

public class AlipayBaseResult {
    private AlipayResultPO alipay_trade_refund_response;

    private String sign;

    public AlipayResultPO getAlipay_trade_refund_response() {
        return alipay_trade_refund_response;
    }

    public void setAlipay_trade_refund_response(AlipayResultPO alipay_trade_refund_response) {
        this.alipay_trade_refund_response = alipay_trade_refund_response;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
