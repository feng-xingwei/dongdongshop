package com.dongdongshop.service;

import com.dongdongshop.pojo.TbContent;

import java.util.List;

public interface IndexContentService {

    List<TbContent> getContentList(Long category);

    List<TbContent> selectAll();
}
