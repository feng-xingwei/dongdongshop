package com.dongdongshop.service;

import com.dongdongshop.data.Result;
import com.dongdongshop.pojo.TbSeller;

public interface SellerService {
    Result addSeller(TbSeller seller);

    Result getSellerList(TbSeller seller);

    Result getSellerBack(String id);

    Result updateSellerStatus(String sellerId, String status);

    TbSeller selectSellerById(String username);
}
