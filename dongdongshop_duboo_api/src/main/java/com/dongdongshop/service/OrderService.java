package com.dongdongshop.service;

import com.dongdongshop.pojo.OrderResult;
import com.dongdongshop.pojo.TbAddress;
import com.dongdongshop.pojo.TbOrder;
import com.dongdongshop.pojo.TbOrderItem;

import java.util.List;

public interface OrderService {
    List<TbAddress> getAddressList(String username);

    TbAddress getAddressById(Long id);

    void addOrder(TbOrder order);

    void addOrderItem(TbOrderItem tbOrderItem);

    boolean addAddress(TbAddress address);

    void payOrderSuccessfully(String out_trade_no, TbOrder order, TbOrderItem orderItem);

    List<OrderResult> getOrderList(String username);

    void deleteOrderItemById(String id);

    void sandMessage(String out_trade_no, TbOrder order, TbOrderItem orderItem, String username);

    List<TbOrder> selectOrderList(String message);
}
