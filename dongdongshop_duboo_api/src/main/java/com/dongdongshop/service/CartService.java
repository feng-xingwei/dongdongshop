package com.dongdongshop.service;

import com.dongdongshop.pojo.Cart;

import java.util.List;

public interface CartService {

    //增加购物车信息
    List<Cart> addCart(List<Cart> cartList, Long itemId, Integer num);

    //将购物车信息存入redis
    void addCartListIntoRedis(List<Cart> cartList, String username);

    //从Redis中获取购物车信息
    List<Cart> getCartListFromRedis(String username);

    //合并cookie与redis数据
    List<Cart> merageCarList(List<Cart> cookieList, List<Cart> redisList);
}
