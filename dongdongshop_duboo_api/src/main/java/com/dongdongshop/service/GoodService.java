package com.dongdongshop.service;

import com.dongdongshop.data.Result;
import com.dongdongshop.pojo.TbGoods;
import com.dongdongshop.pojo.TbGoodsDesc;

import java.math.BigDecimal;

public interface GoodService {
    Result addGood(TbGoods good, TbGoodsDesc goodsDesc, BigDecimal[] price, Integer[] number, String titile2);

    Result getGoodList(String sellerId, TbGoods goods);

    Result getAllGoodList(TbGoods goods);

    Result updateBatch(String[] ids, String status);

    Result deleteBatch(String[] ids);

    TbGoods getGoodByGoodId(Long goodId);

    TbGoodsDesc getGoodDescByGoodId(Long goodId);
}
