package com.dongdongshop.service;

import com.dongdongshop.po.Comment;

import java.util.List;

public interface CommentService {
    void addComment(Comment comment);

    List<Comment> getCommentList(Long goodsId, Integer star);
}
