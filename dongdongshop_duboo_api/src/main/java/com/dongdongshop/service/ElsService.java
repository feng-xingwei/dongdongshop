package com.dongdongshop.service;

import com.dongdongshop.po.SearchResult;

import java.util.List;

public interface ElsService {
    List<SearchResult> getSearchResult(String input);
}
