package com.dongdongshop.service;

import com.dongdongshop.data.Page;
import com.dongdongshop.data.Result;
import com.dongdongshop.pojo.TbItem;
import com.dongdongshop.pojo.TbItemCat;

import java.util.List;

public interface ItemService {
    Page<List<TbItemCat>> getItemList(Long id, Integer pageNum, Integer pageSize);

    Result getItem(Long id);

    Result addItem(TbItemCat itemCat);

    Result toUpdate(Long id);

    Result updateItem(TbItemCat itemCat);

    Result deleteBatch(Long[] ids);

    List<TbItem> getItemsByGoodId(Long goodId);

    TbItem getItemByItemId(Long itemId);
}
