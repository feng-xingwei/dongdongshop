package com.dongdongshop.service;

import com.dongdongshop.pojo.TbContent;
import com.dongdongshop.pojo.TbContentCategory;

import java.util.List;

public interface ContentService {
    List<TbContentCategory> getContentCategoryList(String name);

    boolean addContentCategory(TbContentCategory category);

    TbContentCategory getContentCategoryBack(Long id);

    boolean updateContentCategory(TbContentCategory category);

    List<TbContent> getContentList();

    boolean addContent(TbContent content);

    TbContent getContentBack(Long id);

    boolean updateContent(TbContent content);

    boolean deleteContentBatch(Long[] ids);

    boolean deleteContentCategoryBatch(Long[] ids);

    List<TbContent> getContentList(Long category);
}
