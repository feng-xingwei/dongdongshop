package com.dongdongshop.service;

import com.dongdongshop.pojo.TbTypeTemplate;
import com.dongdongshop.data.Result;

public interface TypeService {
    Result getTypeList(String name);

    Result addType(TbTypeTemplate typeTemplate);

    Result getTypeById(Integer id);

    Result updateType(TbTypeTemplate typeTemplate);

    Result deleteBatch(Long[] ids);
}
