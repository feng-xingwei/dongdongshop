package com.dongdongshop.service;

import com.dongdongshop.pojo.TbUserScore;

public interface UserScoreService {

    void addUserScore(TbUserScore userScore);
}
