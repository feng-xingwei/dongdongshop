package com.dongdongshop.service;

import com.dongdongshop.pojo.TbUser;

public interface UserService {
    TbUser selectUserById(String username);

    boolean register(TbUser user);

    String getUserMail(String userName);

    void rePassword(TbUser user);
}
