package com.dongdongshop.service;

import com.dongdongshop.pojo.TbAddress;
import com.dongdongshop.pojo.TbAreas;
import com.dongdongshop.pojo.TbCities;
import com.dongdongshop.pojo.TbProvinces;

import java.util.List;

public interface AddressService {
    List<TbProvinces> getProvinceList();

    List<TbCities> getCityList(String id);

    List<TbAreas> getAreaList(String id);
}
