package com.dongdongshop.service;

import com.dongdongshop.pojo.TbBrand;
import com.dongdongshop.data.Result;

public interface BrandService {

    Result getBrandList();

    Result selectBrandById(Integer id);

    Result updateBrand(TbBrand tbBrand);

    Result addBrand(TbBrand tbBrand);

    Result deleteBatch(Integer[] ids);
}
