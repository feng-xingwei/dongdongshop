package com.dongdongshop.service;

import com.dongdongshop.pojo.TbSpecification;
import com.dongdongshop.pojo.TbSpecificationOption;
import com.dongdongshop.data.Result;

import java.util.List;

public interface SpecificationService {
    Result getSpecificationList(String specName);

    Result addSpecification(TbSpecification specification, List<TbSpecificationOption> tbSpecificationOptions);

    Result getSpecificationById(Integer id);

    Result updateSpecification(TbSpecification specification, List<TbSpecificationOption> tbSpecificationOptions);

    Result deleteBatch(Integer[] ids);

    Result getSpecNameById(Long specId);

    Result getSpecification(Long id);
}
