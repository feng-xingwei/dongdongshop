package com.dongdongshop.service;

import com.dongdongshop.pojo.TbSeckillGoods;

import java.util.List;

public interface SecKillService {
    List<TbSeckillGoods> getGoodList();

    TbSeckillGoods findSeckillById(Long id);

    void createOrder(Long id, String username) throws Exception;
}
