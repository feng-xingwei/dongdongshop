package com.dongdongshop.pojo;

import java.io.Serializable;
import java.util.Date;

public class TbUserScore implements Serializable {
    private Long id;

    private String userId;

    private Long totalScore;

    private Date createTime;

    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Long getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Long totalScore) {
        this.totalScore = totalScore;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public TbUserScore() {
    }

    public TbUserScore(String userId, Long totalScore, Date createTime, Date updateTime) {
        this.userId = userId;
        this.totalScore = totalScore;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }
}