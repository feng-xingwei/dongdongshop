package com.dongdongshop.pojo;

public class SendOrder {

    private String out_trade_no;
    private TbOrder order;
    private TbOrderItem orderItem;

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public TbOrder getOrder() {
        return order;
    }

    public void setOrder(TbOrder order) {
        this.order = order;
    }

    public TbOrderItem getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(TbOrderItem orderItem) {
        this.orderItem = orderItem;
    }

    public SendOrder() {
    }

    public SendOrder(String out_trade_no, TbOrder order, TbOrderItem orderItem) {
        this.out_trade_no = out_trade_no;
        this.order = order;
        this.orderItem = orderItem;
    }

    public SendOrder(TbOrder order, TbOrderItem orderItem) {
        this.order = order;
        this.orderItem = orderItem;
    }
}
