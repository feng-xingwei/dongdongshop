package com.dongdongshop.pojo;

import java.io.Serializable;

public class TbSpecification implements Serializable {
    private Long id;

    private String specName;

    private String optionName;

    private Integer orders;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName == null ? null : specName.trim();
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public Integer getOrders() {
        return orders;
    }

    public void setOrders(Integer orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "TbSpecification{" +
                "id=" + id +
                ", specName='" + specName + '\'' +
                ", optionName='" + optionName + '\'' +
                ", orders=" + orders +
                '}';
    }
}