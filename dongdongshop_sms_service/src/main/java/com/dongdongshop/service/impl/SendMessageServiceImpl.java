package com.dongdongshop.service.impl;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.dongdongshop.service.SendMessageService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.xml.ws.Action;
import java.util.concurrent.TimeUnit;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class SendMessageServiceImpl implements SendMessageService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void sendMessage(String phone) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-beijing",
                "LTAI4GCbdwnjCGwQj9Z3tr3N", "27ih4pc1W1tqey9X4mmCy1JUotAX7L");
        /** use STS Token
         DefaultProfile profile = DefaultProfile.getProfile(
         "<your-region-id>",           // The region ID
         "<your-access-key-id>",       // The AccessKey ID of the RAM account
         "<your-access-key-secret>",   // The AccessKey Secret of the RAM account
         "<your-sts-token>");          // STS Token
         **/
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        //接受短信者手机号码
        request.putQueryParameter("PhoneNumbers", phone);
        //短信签名
        request.putQueryParameter("SignName", "东科创想");
        //短信模板ID
        request.putQueryParameter("TemplateCode", "SMS_162522027");

        //创建验证码
        String msg = RandomStringUtils.randomNumeric(6);
        //短信模板变量
        request.putQueryParameter("TemplateParam", "{\"sms_code\":\"" + msg + "\"}");
        //存到redis,设置失效时间
        redisTemplate.opsForValue().set(phone, msg, 30, TimeUnit.MINUTES);


        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }
}
