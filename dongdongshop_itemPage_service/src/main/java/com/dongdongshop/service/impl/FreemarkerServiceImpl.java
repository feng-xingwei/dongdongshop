package com.dongdongshop.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.pojo.TbGoods;
import com.dongdongshop.pojo.TbGoodsDesc;
import com.dongdongshop.pojo.TbItem;
import com.dongdongshop.service.FreemarkerService;
import com.dongdongshop.service.GoodService;
import com.dongdongshop.service.ItemService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class FreemarkerServiceImpl implements FreemarkerService {

    @Autowired
    private FreeMarkerConfigurer freeMarkerConfigurer;

    @Reference
    private GoodService goodService;

    @Reference
    private ItemService itemService;

    @Override
    public void createPage(Long goodId) {
        Configuration configuration = freeMarkerConfigurer.getConfiguration();
        FileWriter fileWriter = null;
        try {
            // 第四步：加载一个模板，创建一个模板对象。
            Template template = configuration.getTemplate("item.ftl");
            // 第五步：创建一个模板使用的数据集，可以是 pojo 也可以是 map。一般是 Map
            Map map = new HashMap();
            // 第六步：创建一个 Writer 对象，一般创建一 FileWriter 对象，指定生成的文件名。
            fileWriter = new FileWriter("F:\\ftltest\\" + goodId + ".html");

            //数据填充
            TbGoods goods = goodService.getGoodByGoodId(goodId);
            TbGoodsDesc goodsDesc = goodService.getGoodDescByGoodId(goodId);
            List<TbItem> itemList = itemService.getItemsByGoodId(goodId);
            map.put("goods", goods);
            map.put("goodsDesc", goodsDesc);
            map.put("itemList", itemList);

            // 第七步：调用模板对象的 process 方法输出文件。
            template.process(map, fileWriter);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 第八步：关闭流
            try {
                if(fileWriter != null){
                    fileWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
