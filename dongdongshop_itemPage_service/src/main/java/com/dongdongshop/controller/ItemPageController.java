package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.service.CartService;
import com.dongdongshop.service.FreemarkerService;
import com.dongdongshop.service.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("itemPageController")
public class ItemPageController {

    @Autowired
    private FreemarkerService freemarkerService;

    @Reference
    private CartService cartService;

    @RequestMapping("createPage")
    @ResponseBody
    public String createPage(Long goodId){
        freemarkerService.createPage(goodId);
        return "ok";
    }
}
