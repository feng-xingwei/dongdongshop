package com.dongdongshop.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.pojo.TbSeller;
import com.dongdongshop.service.SellerService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("registerController")
public class RegisterController {

    @Reference
    private SellerService sellerService;

    @RequestMapping("toRegister")
    public String toRegister(){
        return "register";
    }

    @RequestMapping("register")
    @ResponseBody
    public Result register(TbSeller seller){
        seller.setStatus("0");
        Result result = sellerService.addSeller(seller);
        return result;
    }
}
