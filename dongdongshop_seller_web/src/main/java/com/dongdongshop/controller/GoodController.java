package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dongdongshop.data.Page;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.*;
import com.dongdongshop.service.*;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("goodController")
public class GoodController {

    @Reference
    private ItemService itemService;

    @Reference
    private TypeService typeService;

    @Reference
    private GoodService goodService;

    @Reference
    private SpecificationService specificationService;

    @RequestMapping("toAddGood")
    public String toAddGood(Model model){
        return "admin/goods_edit";
    }

    @RequestMapping("toGood")
    public String toGood(){
        return "admin/goods";
    }

    //获取分类
    @RequestMapping("getItemList")
    @ResponseBody
    public Result getItemList(@RequestParam(defaultValue = "0") Long id, @RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "99") Integer pageSize){
        Page<List<TbItemCat>> page = itemService.getItemList(id, pageNum, pageSize);
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(page);
    }

    //获取规格
    @RequestMapping("getSpecNameById/{specId}")
    @ResponseBody
    public Result getSpecNameById(@PathVariable("specId")Long specId){
        Result result = specificationService.getSpecNameById(specId);
        return result;
    }

    //获取模板中的品牌字符串
    @RequestMapping("getType/{id}")
    @ResponseBody
    public Result getType(@PathVariable("id")Integer id){
        Result result = typeService.getTypeById(id);
        return result;
    }

    @RequestMapping("getGoodList")
    @ResponseBody
    public Result getGoodList(TbGoods goods){
        TbSeller seller = (TbSeller) SecurityUtils.getSubject().getPrincipal();
        String sellerId = seller.getSellerId();
        Result result = goodService.getGoodList(sellerId, goods);
        return result;
    }

    @RequestMapping("addGood")
    @ResponseBody
    public Result addGood(TbGoods good, TbGoodsDesc goodsDesc, BigDecimal[] price2, Integer[] number, String titile2){

        TbSeller seller = (TbSeller) SecurityUtils.getSubject().getPrincipal();
        good.setSellerId(seller.getSellerId());
        good.setAuditStatus("0");;

        Result result = goodService.addGood(good, goodsDesc, price2, number, titile2);
        return result;
    }

    //提交审核
    @RequestMapping("updateBatch")
    @ResponseBody
    public Result updateBatch(String[] ids){
        Result result = goodService.updateBatch(ids, "3");
        return result;
    }

    //批量屏蔽
    @RequestMapping("deleteBatch")
    @ResponseBody
    public Result deleteBatch(String[] ids){
        Result result = goodService.deleteBatch(ids);
        return result;
    }

    @RequestMapping("getSpec/{id}")
    @ResponseBody
    public Result getSpec(@PathVariable("id")Integer id){
        ArrayList<VO> vos = new ArrayList<>();
        Result result = typeService.getTypeById(id);
        TbTypeTemplate typeTemplate = (TbTypeTemplate) result.getResult();
        List<TbSpecification> specificationList = JSONArray.parseArray(typeTemplate.getSpecIds(), TbSpecification.class);
        for (TbSpecification spec: specificationList) {
            Result specificationById = specificationService.getSpecification((spec.getId()));
            TbSpecification specification = (TbSpecification) specificationById.getResult();
            VO vo = new VO();
            ArrayList<TbSpecificationOption> specOptions = new ArrayList<>();
            vo.setSpecification(specification);
            Result optionList = specificationService.getSpecNameById(spec.getId());
            List<TbSpecificationOption> specificationOptionList = (List<TbSpecificationOption>)optionList.getResult();
            for (TbSpecificationOption specOption : specificationOptionList){
                specOptions.add(specOption);
            }
            vo.setSpecificationOptionList(specOptions);
            vos.add(vo);
        }
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(vos);
    }
}
