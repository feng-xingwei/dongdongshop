package com.dongdongshop.controller;

import com.dongdongshop.exception.AuditFailed;
import com.dongdongshop.exception.Closed;
import com.dongdongshop.exception.NotAudit;
import com.sun.jmx.snmp.SnmpUnknownAccContrModelException;
import org.apache.catalina.security.SecurityUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("loginController")
public class LoginController {

    @RequestMapping("toLogin")
    public String toLogin(){
        return "shoplogin";
    }

    @RequestMapping("login")
    public String login(String sellerId, String password, Model model){
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(sellerId, password);
        try {
            subject.login(token);
            return "admin/index";
        } catch (UnknownAccountException e){
            model.addAttribute("msg", "用户名不存在");
            return "shoplogin";
        } catch (IncorrectCredentialsException e){
            model.addAttribute("msg", "密码错误");
            return "shoplogin";
        } catch (NotAudit e){
            model.addAttribute("msg", "商家未审核");
            return "shoplogin";
        } catch (AuditFailed e){
            model.addAttribute("msg", "商家审核未通过");
            return "shoplogin";
        } catch (Closed e){
            model.addAttribute("msg", "商家已关闭");
            return "shoplogin";
        }
    }

    @RequestMapping("outLogin")
    public String outLogin(){
        return "shoplogin";
    }
}
