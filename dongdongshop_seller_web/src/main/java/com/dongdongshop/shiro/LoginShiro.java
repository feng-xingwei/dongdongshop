package com.dongdongshop.shiro;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.exception.AuditFailed;
import com.dongdongshop.exception.Closed;
import com.dongdongshop.exception.NotAudit;
import com.dongdongshop.pojo.TbSeller;
import com.dongdongshop.service.SellerService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

public class LoginShiro extends AuthorizingRealm {

    @Reference
    private SellerService sellerService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }

    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        TbSeller seller = sellerService.selectSellerById(token.getUsername());
        if(seller == null){
            return null;
        }
        if("0".equals(seller.getStatus())){
            throw new NotAudit();
        }
        if("2".equals(seller.getStatus())){
            throw new AuditFailed();
        }
        if ("3".equals(seller.getStatus())){
            throw new Closed();
        }
        return new SimpleAuthenticationInfo(seller, seller.getPassword(), seller.getSellerId());
    }
}
