package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.dongdongshop.pojo.TbOrder;
import com.dongdongshop.pojo.TbOrderItem;
import com.dongdongshop.pojo.TbUser;
import com.dongdongshop.service.OrderService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.dongdongshop.controller.TradeController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Controller
@RequestMapping("alipayCallBack")
public class AlipayCallBack {

    @Autowired
    private RedisTemplate redisTemplate;

    @Reference
    private OrderService orderService;

    /*
     *  @param out_trade_no 订单号
     *  @param trade_no 交易流水号
     *  @param total_amount 支付金额
     * */
    //同步回调
    @RequestMapping("returnUrl")
    public String returnUrl(HttpServletRequest request, String out_trade_no, String trade_no, String total_amount, Model model) throws AlipayApiException {
        //获取支付宝GET过来反馈信息
        Map<String,String> params = new HashMap<String,String>();
        Map<String,String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }

        boolean signVerified = AlipaySignature.rsaCheckV1(params, com.dongdongshop.config.AlipayConfig.alipay_public_key, com.dongdongshop.config.AlipayConfig.charset, com.dongdongshop.config.AlipayConfig.sign_type); //调用SDK验证签名

        //——请在这里编写您的程序（以下代码仅作参考）——
        System.out.println("进入同步回调方法>>>>>>>>");
        if(signVerified) {
            System.out.println("同步验签成功");
            model.addAttribute("trade_no",trade_no);
            model.addAttribute("out_trade_no",out_trade_no);
            model.addAttribute("total_amount",total_amount);
            return "paysuccess";
        }else {
            System.out.println("同步验签失败");
            return "payfail";
        }
    }


    /*
    *  @param out_trade_no 订单号
    *  @param trade_no 交易流水号
    *  @param trade_status 支付状态
    * */
    //异步通知
    @RequestMapping("notifyUrl")
    @ResponseBody
    public String notifyUrl(HttpServletRequest request, String out_trade_no, String trade_no, String trade_status) throws AlipayApiException {
        //获取支付宝POST过来反馈信息
        Map<String,String> params = new HashMap<String,String>();
        Map<String,String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用
//            valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }

        //验证数字签名
        boolean signVerified = AlipaySignature.rsaCheckV1(params, com.dongdongshop.config.AlipayConfig.alipay_public_key, com.dongdongshop.config.AlipayConfig.charset, com.dongdongshop.config.AlipayConfig.sign_type); //调用SDK验证签名

        //——请在这里编写您的程序（以下代码仅作参考）——

	/* 实际验证过程建议商户务必添加以下校验：
	1、需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
	2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
	3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
	4、验证app_id是否为该商户本身。
	*/
        System.out.println("进入异步回调方法>>>>>>>>");
        if(signVerified) {//验证成功
            if(trade_status.equals("TRADE_FINISHED")){  //用户取消支付
                //关闭交易
                TradeController tradeController = new TradeController();
                tradeController.toAlipayTradeClose(out_trade_no, trade_no);
            }else if (trade_status.equals("TRADE_SUCCESS")){    //用户 支付成功, 退款成功 都会进入该方法
                //判断支付成功还是退款成功
                String[] gmt_refunds = requestParams.get("gmt_refund");
                if(gmt_refunds == null){    //支付成功
                    //判断该笔订单是否已经做过处理(解决幂等性问题)
                    if(!redisTemplate.boundHashOps("alipay").hasKey(trade_no + "trade")){    //没有处理过
                        TbOrder order = new TbOrder();
                        order.setTradeNo(trade_no);
                        order.setStatus("2");
                        order.setUpdateTime(new Date());
                        order.setPaymentTime(new Date());
                        TbOrderItem orderItem = new TbOrderItem();
                        orderItem.setTradeNo(trade_no);
//                        orderService.payOrderSuccessfully(out_trade_no, order, orderItem);
                        TbUser user = (TbUser) SecurityUtils.getSubject().getPrincipal();
                        orderService.sandMessage(out_trade_no, order, orderItem, "fxw");
                        redisTemplate.boundHashOps("alipay").put(trade_no + "trade", 1);
                    }
                }else{ //退款成功
                    String[] out_biz_nos = requestParams.get("out_biz_no");
                    String id = out_biz_nos[0];
                    if(!redisTemplate.boundHashOps("alipay").hasKey(id + "gmt_refund")){
                        orderService.deleteOrderItemById(id);
                        redisTemplate.boundHashOps("alipay").put(id + "gmt_refund", 1);
                    }
                }
            }

            System.out.println("异步回调验签成功");
            return "success";

        }else {//验证失败
            System.out.println("异步回调验签失败");
            return "fail";

            //调试用，写文本函数记录程序运行情况是否正常
            //String sWord = AlipaySignature.getSignCheckContentV1(params);
            //AlipayConfig.logResult(sWord);
        }

        //——请在这里编写您的程序（以上代码仅作参考）——
    }


}
