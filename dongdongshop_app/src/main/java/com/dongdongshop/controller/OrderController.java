package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.*;
import com.dongdongshop.service.AddressService;
import com.dongdongshop.service.CartService;
import com.dongdongshop.service.OrderService;
import com.dongdongshop.util.IdWorker;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("orderController")
public class OrderController {

    @Reference
    private OrderService orderService;

    @Reference
    private AddressService addressService;

    @Reference
    private CartService cartService;

    @Autowired
    private RedisTemplate redisTemplate;

    //跳转结算页面
    @RequestMapping("toOrder")
    public String toOrder(Model model) {
        List<TbProvinces> provinces = addressService.getProvinceList();
        model.addAttribute("provinces", provinces);
        return "getOrderInfo";
    }

    //跳转订单页面
    @RequestMapping("toIndex")
    public String toIndex(){
        return "home-index";
    }

    //跳转支付页面
    @RequestMapping("toPay")
    public String toPay(){
        return "pay";
    }

    //跳转我的订单页面
    @RequestMapping("toOrderIndex")
    public String toOrderIndex(){
        return "home-index";
    }


    //查询收货人信息
    @RequestMapping("getAddressList")
    @ResponseBody
    public Result getAddressList(){
        TbUser user = (TbUser) SecurityUtils.getSubject().getPrincipal();
        List<TbAddress> addressList = orderService.getAddressList(user.getUsername());
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(addressList);
    }

    //增加收货人地址
    @RequestMapping("addAddress")
    @ResponseBody
    public Result addAddress(TbAddress address){
        TbUser user = (TbUser) SecurityUtils.getSubject().getPrincipal();
        address.setUserId(user.getUsername());
        System.out.println(address);
        boolean key = orderService.addAddress(address);
        return Result.build(ConstantEnum.SUCCESS_CREATE);
    }

    //创建订单
    @RequestMapping("createOrder")
    @ResponseBody
    public Result createOrder(Long addressId, Long[] payment){
        TbAddress address = orderService.getAddressById(addressId);
        TbUser user = (TbUser) SecurityUtils.getSubject().getPrincipal();
        //从redis中获取购物车信息
        List<Cart> redisCartList = cartService.getCartListFromRedis(user.getUsername());
        IdWorker idWorker = new IdWorker();
        String outTradeNo = Long.toString(idWorker.nextId());
        for (int i = 0; i < redisCartList.size(); i++){
            //创建商家order订单
            Cart cart = redisCartList.get(i);
            TbOrder order = new TbOrder();
            long orderId = idWorker.nextId();
            order.setOrderId(orderId);
            order.setOutTradeNo(outTradeNo);
            order.setPayment(new BigDecimal(payment[i]));
            order.setCreateTime(new Date());
            order.setUpdateTime(new Date());
            order.setUserId(user.getUsername());
            order.setReceiverMobile(address.getMobile());
            order.setReceiverAreaName(address.getAddress());
            order.setReceiver(address.getContact());
            order.setSellerId(cart.getSellerId());
            orderService.addOrder(order);
            List<TbOrderItem> orderItemList = cart.getOrderItemList();
            for (TbOrderItem tbOrderItem : orderItemList) {
                //创建商品orderItem订单
                tbOrderItem.setId(idWorker.nextId());
                tbOrderItem.setOutTradeNo(outTradeNo);
                tbOrderItem.setOrderId(orderId);
                orderService.addOrderItem(tbOrderItem);
            }
        }
        return Result.build(ConstantEnum.SUCCESS_CREATE).setResult(outTradeNo);
    }

    //查询订单列表
    @RequestMapping("getOrderList")
    @ResponseBody
    public Result getOrderList(){
        TbUser user = (TbUser) SecurityUtils.getSubject().getPrincipal();
        List<OrderResult> orderItemList = orderService.getOrderList(user.getUsername());
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(orderItemList);
    }

    @RequestMapping("getCity/{id}")
    @ResponseBody
    public Result getCity(@PathVariable("id")String id){
        List<TbCities> cityList = addressService.getCityList(id);
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(cityList);
    }

    @RequestMapping("getArea/{id}")
    @ResponseBody
    public Result getArea(@PathVariable("id")String id){
        List<TbAreas> areaList = addressService.getAreaList(id);
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(areaList);
    }
}
