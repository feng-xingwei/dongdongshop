package com.dongdongshop.controller;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.*;
import com.dongdongshop.config.AlipayConfig;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.po.AlipayBaseResult;
import com.dongdongshop.util.IdWorker;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("tradeController")
public class TradeController {

    @RequestMapping("toTrade")
    public String toTrade(){
        return "trade";
    }

    /*
    *  @param WIDout_trade_no 订单号
    *  @param WIDtotal_amount 支付金额
    *  @param WIDsubject 订单名称
    *  @param WIDbody 描述
    * */
    //跳转支付页面
    @RequestMapping("toAlipayTradePagePay")
    @ResponseBody
    public String toAlipayTradePagePay(String WIDout_trade_no, String WIDtotal_amount,
                                       String WIDsubject, String WIDbody) throws AlipayApiException {

        WIDsubject = "支付订单";
        WIDbody = "";

        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(com.dongdongshop.config.AlipayConfig.gatewayUrl, com.dongdongshop.config.AlipayConfig.app_id, com.dongdongshop.config.AlipayConfig.merchant_private_key, "json", com.dongdongshop.config.AlipayConfig.charset, com.dongdongshop.config.AlipayConfig.alipay_public_key, com.dongdongshop.config.AlipayConfig.sign_type);

        //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(com.dongdongshop.config.AlipayConfig.return_url);
        alipayRequest.setNotifyUrl(com.dongdongshop.config.AlipayConfig.notify_url);

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ WIDout_trade_no +"\","
                + "\"total_amount\":\""+ WIDtotal_amount +"\","
                + "\"subject\":\""+ WIDsubject +"\","
                + "\"body\":\""+ WIDbody +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        //请求支付宝
        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //输出
        return result;
    }

    /*
     *  @param WIDTQout_trade_no 订单号
     *  @param WIDTQtrade_no 流水号
     * */
    //跳转订单查询
    @RequestMapping("toAlipayTradeQuery")
    @ResponseBody
    public Result toAlipayTradeQuery(String WIDTQout_trade_no, String WIDTQtrade_no){
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);

        //设置请求参数
        AlipayTradeQueryRequest alipayRequest = new AlipayTradeQueryRequest();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ WIDTQout_trade_no +"\","+"\"trade_no\":\""+ WIDTQtrade_no +"\"}");

        //请求
        String result = null;
        try {
            result = alipayClient.execute(alipayRequest).getBody();
            return Result.build(ConstantEnum.SUCCESS).setResult(result);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            return Result.build(ConstantEnum.ERROR);
        }
    }

    /*
     *  @param WIDTRout_trade_no 订单号
     *  @param WIDTRtrade_no 流水号
     *  @param WIDTRrefund_amount 需要退款的金额，该金额不能大于订单金额，必填
     *  @param WIDTRrefund_reason 退款的原因说明
     *  @param WIDTRout_request_no 标识一次退款请求，同一笔交易多次退款需要保证唯一，如需部分退款，则此参数必传
     * */
    //退款
    @RequestMapping("toAlipayTradeRefund")
    @ResponseBody
    public Result toAlipayTradeRefund(String WIDTRout_trade_no, String WIDTRtrade_no,
                                      String WIDTRrefund_amount, String WIDTRrefund_reason, String WIDTRout_request_no){
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);

        //设置请求参数
        AlipayTradeRefundRequest alipayRequest = new AlipayTradeRefundRequest();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ WIDTRout_trade_no +"\","
                + "\"trade_no\":\""+ WIDTRtrade_no +"\","
                + "\"refund_amount\":\""+ WIDTRrefund_amount +"\","
                + "\"refund_reason\":\""+ WIDTRrefund_reason +"\","
                + "\"out_request_no\":\""+ WIDTRout_request_no +"\"}");

        //请求
        String result = null;
        try {
            result = alipayClient.execute(alipayRequest).getBody();
            AlipayBaseResult alipayBaseResult = JSONObject.parseObject(result, AlipayBaseResult.class);
            return Result.build(ConstantEnum.SUCCESS).setResult(alipayBaseResult);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            return Result.build(ConstantEnum.ERROR);
        }
    }

    /*
     *  @param WIDRQout_trade_no 订单号
     *  @param WIDRQtrade_no 流水号
     *  @param WIDRQout_request_no 请求退款接口时，传入的退款请求号，如果在退款请求时未传入，则该值为创建交易时的外部交易号，必填
     * */
    //退款查询
    @RequestMapping("toAlipayTradeFastpayRefundQuery")
    @ResponseBody
    public Result toAlipayTradeFastpayRefundQuery(String WIDRQout_trade_no, String WIDRQtrade_no, String WIDRQout_request_no){
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);

        //设置请求参数
        AlipayTradeFastpayRefundQueryRequest alipayRequest = new AlipayTradeFastpayRefundQueryRequest();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ WIDRQout_trade_no +"\","
                +"\"trade_no\":\""+ WIDRQtrade_no +"\","
                +"\"out_request_no\":\""+ WIDRQout_request_no +"\"}");

        //请求
        String result = null;
        try {
            result = alipayClient.execute(alipayRequest).getBody();
            return Result.build(ConstantEnum.SUCCESS).setResult(result);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            return Result.build(ConstantEnum.ERROR);
        }
    }

    /*
     *  @param WIDTCout_trade_no 订单号
     *  @param WIDTCtrade_no 流水号
     * */
    //交易关闭
    @RequestMapping("toAlipayTradeClose")
    @ResponseBody
    public Result toAlipayTradeClose(String WIDTCout_trade_no, String WIDTCtrade_no){
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);

        //设置请求参数
        AlipayTradeCloseRequest alipayRequest = new AlipayTradeCloseRequest();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ WIDTCout_trade_no +"\"," +"\"trade_no\":\""+ WIDTCtrade_no +"\"}");

        //请求
        String result = null;
        try {
            result = alipayClient.execute(alipayRequest).getBody();
            return Result.build(ConstantEnum.SUCCESS).setResult(result);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            return Result.build(ConstantEnum.ERROR);
        }
    }
}
