package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.TbContent;
import com.dongdongshop.service.IndexContentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("contentController")
public class ContentController {

    @Reference
    private IndexContentService contentService;

    @RequestMapping(path = {"/", "index"})
    public String toIndex(){
        return "index";
    }

    @RequestMapping("getIndexContent/{category}")
    @ResponseBody
    public Result getIndexContent(@PathVariable("category")Long category){
        List<TbContent> contentList = contentService.getContentList(category);
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(contentList);
    }
}
