package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.po.SearchResult;
import com.dongdongshop.service.ElsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("elsController")
public class ElsController {

    @Reference
    private ElsService elsService;


    @RequestMapping("toSearch")
    public String toSearch(String input, Model model){
        List<SearchResult> searchList = elsService.getSearchResult(input);
        System.out.println(searchList);
        model.addAttribute("searchList", searchList);
        return "search";
    }
}
