package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.Cart;
import com.dongdongshop.pojo.TbUser;
import com.dongdongshop.service.CartService;
import com.dongdongshop.util.CookieUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@CrossOrigin
@RequestMapping("cartController")
public class CartController {

    @Reference
    private CartService cartService;


    //跳转购物车页面
    @RequestMapping("toCart")
    public String toCart(){
        return "cart";
    }

    //获取购物车信息
    @RequestMapping("getCart")
    @ResponseBody
    public List<Cart> getCart(HttpServletRequest request, HttpServletResponse response){

        //获取Cookie里的信息
        String cookieValue = CookieUtils.getCookieValue(request, "cartList", true);
        if(StringUtils.isBlank(cookieValue)){
            cookieValue = "[]";
        }
        //将CookieJson字符串转成对象
        List<Cart> cookieCartList = JSONObject.parseArray(cookieValue, Cart.class);

        //判断用户是否登录
        TbUser user = (TbUser) SecurityUtils.getSubject().getPrincipal();

        if(user == null){ //用户未登录 --- 从cookie获取购物车信息
            return cookieCartList;
        } else { //用户已登录 --- 先从redis获取购物车信息,再合并cookie与redis信息
            //从redis中获取购物车信息
            List<Cart> redisCartList = cartService.getCartListFromRedis(user.getUsername());
            //判断cookie中是否有数据, 有就合并, 没有直接返回redis数据
            if (cookieCartList.size() > 0){
                //合并购物车信息
                redisCartList = cartService.merageCarList(cookieCartList, redisCartList);
                //将合并后购物车信息再次存入redis
                cartService.addCartListIntoRedis(redisCartList, user.getUsername());
                //清空cookie中的数据
                CookieUtils.deleteCookie(request, response, "cartList");
                return redisCartList;
            }
            return redisCartList;
        }
    }

    //添加购物车信息
    @RequestMapping("addCart")
    @ResponseBody
    public Result addCart(HttpServletRequest request, HttpServletResponse response, Long itemId, Integer num){
        //获取购物车信息
        List<Cart> cartList = getCart(request, response);
        //添加购物车信息
        cartList = cartService.addCart(cartList, itemId, num);
        //获取登录用户
        TbUser user = (TbUser) SecurityUtils.getSubject().getPrincipal();
        if(user == null){ //用户未登录
            //将购物车信息放入Cookie
            String cookieValue = JSONObject.toJSONString(cartList);
            CookieUtils.setCookie(request, response, "cartList", cookieValue, true);
        } else { //用户已登录
            //将购物车信息存入redis
            cartService.addCartListIntoRedis(cartList, user.getUsername());
        }
        return Result.build(ConstantEnum.SUCCESS_CREATE);
    }
}
