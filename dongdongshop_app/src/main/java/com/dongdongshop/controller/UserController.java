package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.pojo.TbUser;
import com.dongdongshop.service.UserService;
import com.dongdongshop.util.ShiroUtils;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.omg.CORBA.Object;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Objects;

@Controller
@RequestMapping("userController")
public class UserController {

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    @Reference
    private UserService userService;

    //跳转用户登录页面
    @RequestMapping("toLogin")
    public String toLogin(){
        return "login";
    }

    //用户登录
    @RequestMapping("/login")
    public String login(String username, String password, Model model){
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        try {
            subject.login(token);
            return "index";
        } catch (UnknownAccountException e){
            model.addAttribute("msg", "用户名不存在");
            return "login";
        } catch (IncorrectCredentialsException e){
            model.addAttribute("msg", "用户名或密码错误");
            return "login";
        }
    }

    //退出登录
    @RequestMapping("outLogin")
    public String outLogin(){
        return "login";
    }

    //跳转注册页面
    @RequestMapping("toRegister")
    public String toRegister(){
        return "register";
    }

    //获取验证码
    @RequestMapping("getMessage/{phone}")
    @ResponseBody
    public Result getMessage(@PathVariable("phone")String phone){
        rocketMQTemplate.convertAndSend("phoneNum", phone);
        return Result.build(ConstantEnum.SUCCESS);
    }

    //注册
    @RequestMapping("register")
    @ResponseBody
    public Result register(String username, String password, String phone, String code){
        if(!redisTemplate.hasKey(phone)){
            return Result.build(ConstantEnum.ERROR);
        }
        String code2 = (String) redisTemplate.opsForValue().get(phone);
        if(!code2.equals(code)){
            return Result.build(ConstantEnum.ERROR);
        }

        //获取随机盐
        String salt = ShiroUtils.generateSalt(6);
        //获取加密密文
        String password2 = ShiroUtils.encryptPassword("MD5", password, salt, 1);

        TbUser user = new TbUser();
        user.setUsername(username);
        user.setPhone(phone);
        user.setSalt(salt);
        user.setPassword(password2);
        user.setCreated(new Date());
        user.setUpdated(new Date());
        boolean key = userService.register(user);
        return Result.build(ConstantEnum.SUCCESS);
    }
}
