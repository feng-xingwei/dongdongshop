package com.dongdongshop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dongdongshop.data.Result;
import com.dongdongshop.em.ConstantEnum;
import com.dongdongshop.po.Comment;
import com.dongdongshop.pojo.TbUser;
import com.dongdongshop.service.CommentService;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("commentController")
@CrossOrigin
public class CommentController {

    @Reference
    private CommentService commentService;

    //增加评价
    @RequestMapping("addComment")
    @ResponseBody
    public Result addComment(Comment comment){
        TbUser user = (TbUser) SecurityUtils.getSubject().getPrincipal();
        comment.setUserId(user.getUsername());
        comment.setDate(new Date());
        commentService.addComment(comment);
        return Result.build(ConstantEnum.SUCCESS_CREATE);
    }

    //查询评价
    @RequestMapping("getComment")
    @ResponseBody
    public Result getComment(Long goodsId, Integer star){
        List<Comment> commentList = commentService.getCommentList(goodsId, star);
        return Result.build(ConstantEnum.SUCCESS_RETRIEVE).setResult(commentList);
    }
}
