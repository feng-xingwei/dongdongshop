package com.dongdongshop.config;

import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {

//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2021000118620187";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCnBoRGIJkHrBwjPKwjgmISzcyZRih4KPAqJqKMc/fCD5QMvvqVMjZejJ8OQF3KzSuVbHx87irEiBxxAqhrlAzB4M3QOtVRpf1zPJmuZ2/1q+K0aF+uQWKRz+PPym0bdUuKcguvgk0eDuVyzeDXtqs/ubNqgsFBa8i8oHsVXWXyPPhAYDx11oN2g8nt/STuEEG9Tf2yVuD/9hsFY0E8Rf4rpGI5zObvxzh1JuVlHxjGcQJ2ImTV0YN9MbKSHvDQH+baM2Am3k7ayhM0Bv7C3UxVdJhryxr5ibkv4gFPjV9hnK3aR6/mm4Ho+rXOrlvRKaxqlD3hyD/BLKg1SvDfB9iTAgMBAAECggEAB9TtZOBobIyscRaKN8wITztpKZK5mRDKWh+fpZhWeLoERii6VT6agAp19tQb0cOXFJWWrMD6Lmb3dnEadpjllD23SNlJYV+4J6wixKPDAzEBG1Z9jg0w0wd9diPWw9/XH/HDDIK+nPcz2RNARL0jAITznb3t0+R68CVSQCk1+4q+gj+PnV1cU/Qc9ZxjrjPuVDOevg46JuJuPOeGA6869JX+Sl6kBcZt6q8+rCRlCU6oLbErHY+i21O22ac9gEHobzUHw9xks6ze6/QKVbGYadWB9vADXFoK4IiWRxHYpNxnK20PhTH27xq8kS08HrUv0wWiTq8mevV18Sp5HPxRyQKBgQD/SToZPg/NRTRNXs75Dmzo3sNsrYqdMIdMTQw4b+97K3h+v8rNJowuszPBTaDdHgb9W0RodrvQoLSy0F7ZJKWc9YthU2taDOyOK/cxmx6+SXoLcuDpjYSW8S9o8WPLM4LX84jBDkhueWbhg7d5/4HpVB7d+Aalpp0q9DchcztifQKBgQCnfhlnM7I+dJXN45+xh92eAJ4l6fnQ6/VWOzviIHTy0Bund0PL8YNfoweaxVj69CpADoJIHosK1fFc8iTL/IAlT+p54NkgSMcPgEaXxzLCE9DqElM3o7fjzPIsbnro1/VYSjG7wcqcDWMKCFX+Oy3B52+ECp/EFLvmVZ6TPFyETwKBgGDUS58rdR7s8uut3F+HYzPo2sMsB/mbYJZGWy5E9tcHJAsfgDLUPjOwcR8O7McyDoCmz4m4D4EQk7mHY5KFXfPtBtMBqyRa5pb+giJkrgKLKCmTh0/PB82g6dSqSqnWffifPO3NFxggIC/FoCA8E/CGobabzxmh2tdiDk83v/yZAoGARWhV8pRTqtn5Mia/iHv69HzhDGkXIuKE+kT1ndFBZVWkABPGPn9Ip5sRjat2IhiIYZ8hke1GRSk/KfsWEIoEOHITgooQr/Y4t0268y6S9CXuDvGlFot7qqTFPnBR8qd7fijQzzORWHWbsw6dRAsV/SKA7cDRUBofe2XDntQGqrUCgYAcKTitFbbFkLxE7SBEnBWbb4r8Q7DJmIqDdJkdQAFHSFIUNt0oVfRmSuopCw3IddOFHo/Voxksqx4r5KyjL4IJTISi3o4te+PsbxhENXh9+11WIIxwMU9ha1OMHbev4cd0BaXop6BdzrCKpDaE42jJ0j1aEjahKtpkx1lFsIWnnA==\n";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjTKxEulreQSS8oDxVWyYJjCxscfSNxqlEus+qDGkxPgDED5gXUtdK2TlUK7S3nmcYmbRaM/gbaAkOwhbI537s5a7UeMzsBigZ8lJRpPufCsHB2tDyRH4K81Ae8Y9ub7tVtnjaGVhAGFf/qfuCPTmn3NsprEa5IcHCtjFfhs1jAXY4X/QM881md9wiuFGgYZghnubNEFjbSB4BjEr4iz1w9XqFihUEO5rz8hIBRlBkHoKZx7wWy0f2erGyK8M7Yqveb0jWfRJYKYyBofrFYCQKQ6de+SH5WqsMgBryA1SDpw9sDFTAnGOhVKVHUBjG/XWe8/Pkb4IqthjEpp7e2JaHQIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://j463733197.imdo.co/alipayCallBack/notifyUrl";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://j463733197.imdo.co/alipayCallBack/returnUrl";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 日志打印位置
    public static String log_path = "E:\\logs";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}