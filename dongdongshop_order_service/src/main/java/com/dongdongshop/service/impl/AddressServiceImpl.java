package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbAddressMapper;
import com.dongdongshop.mapper.TbAreasMapper;
import com.dongdongshop.mapper.TbCitiesMapper;
import com.dongdongshop.mapper.TbProvincesMapper;
import com.dongdongshop.pojo.*;
import com.dongdongshop.service.AddressService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class AddressServiceImpl implements AddressService {

    @Resource
    private TbProvincesMapper provincesMapper;
    @Resource
    private TbCitiesMapper citiesMapper;
    @Resource
    private TbAreasMapper areasMapper;

    @Override
    public List<TbProvinces> getProvinceList() {
        return provincesMapper.selectByExample(null);
    }

    @Override
    public List<TbCities> getCityList(String id) {
        TbCitiesExample tbCitiesExample = new TbCitiesExample();
        TbCitiesExample.Criteria criteria = tbCitiesExample.createCriteria();
        criteria.andProvinceidEqualTo(id);
        return citiesMapper.selectByExample(tbCitiesExample);
    }

    @Override
    public List<TbAreas> getAreaList(String id) {
        TbAreasExample tbAreasExample = new TbAreasExample();
        TbAreasExample.Criteria criteria = tbAreasExample.createCriteria();
        criteria.andCityidEqualTo(id);
        return areasMapper.selectByExample(tbAreasExample);
    }
}
