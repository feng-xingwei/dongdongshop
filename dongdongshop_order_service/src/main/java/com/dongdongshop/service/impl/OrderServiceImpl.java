package com.dongdongshop.service.impl;

import com.dongdongshop.mapper.TbAddressMapper;
import com.dongdongshop.mapper.TbOrderItemMapper;
import com.dongdongshop.mapper.TbOrderMapper;
import com.dongdongshop.pojo.*;
import com.dongdongshop.service.OrderService;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class OrderServiceImpl implements OrderService {

    @Resource
    private TbAddressMapper addressMapper;

    @Resource
    private TbOrderMapper orderMapper;

    @Resource
    private TbOrderItemMapper orderItemMapper;

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    //获取收货人信息
    @Override
    public List<TbAddress> getAddressList(String username) {
        TbAddressExample tbAddressExample = new TbAddressExample();
        TbAddressExample.Criteria criteria = tbAddressExample.createCriteria();
        criteria.andUserIdEqualTo(username);
        List<TbAddress> tbAddresses = addressMapper.selectByExample(tbAddressExample);
        return tbAddresses;
    }

    //增加收货人信息
    @Override
    public boolean addAddress(TbAddress address) {
        int i = addressMapper.insertSelective(address);
        return i > 0;
    }

    //获取选中收货地址
    @Override
    public TbAddress getAddressById(Long id) {
        TbAddressExample tbAddressExample = new TbAddressExample();
        TbAddressExample.Criteria criteria = tbAddressExample.createCriteria();
        criteria.andIdEqualTo(id);
        List<TbAddress> tbAddresses = addressMapper.selectByExample(tbAddressExample);
        return (tbAddresses==null ? null : tbAddresses.get(0));
    }

    @Override
    public void addOrder(TbOrder order) {
        orderMapper.insertSelective(order);
    }

    @Override
    public void addOrderItem(TbOrderItem tbOrderItem) {
        orderItemMapper.insertSelective(tbOrderItem);
    }

    //消息提供者
    @Override
    public void sandMessage(String out_trade_no, TbOrder order, TbOrderItem orderItem, String username) {
        //创建本地事务回调参数
        SendOrder sendOrder = new SendOrder(out_trade_no, order, orderItem);
        //创建发送信息
        //Date date = new Date();
        //Message<TbUserScore> message = MessageBuilder.withPayload(new TbUserScore(username, 2000L, date, date)).build();
        Message<String> message = MessageBuilder.withPayload(out_trade_no).build();
        rocketMQTemplate.sendMessageInTransaction("tx-group", "tx-topic", message, sendOrder);
    }

    //用户成功支付,修改订单
    @Override
    public void payOrderSuccessfully(String out_trade_no, TbOrder order, TbOrderItem orderItem) {
        TbOrderExample orderExample = new TbOrderExample();
        TbOrderExample.Criteria criteria = orderExample.createCriteria();
        criteria.andOutTradeNoEqualTo(out_trade_no);
        orderMapper.updateByExampleSelective(order, orderExample);

        TbOrderItemExample orderItemExample = new TbOrderItemExample();
        TbOrderItemExample.Criteria criteria1 = orderItemExample.createCriteria();
        criteria1.andOutTradeNoEqualTo(out_trade_no);
        orderItemMapper.updateByExampleSelective(orderItem, orderItemExample);
    }

    //获取订单列表
    @Override
    public List<OrderResult> getOrderList(String username) {
        List<OrderResult> orderResultList = new ArrayList<>();

        TbOrderExample orderExample = new TbOrderExample();
        TbOrderExample.Criteria criteria = orderExample.createCriteria();
        criteria.andUserIdEqualTo(username);
        List<TbOrder> orders = orderMapper.selectByExample(orderExample);

        for (TbOrder order : orders) {
            OrderResult orderResult = new OrderResult();
            orderResult.setOrder(order);

            TbOrderItemExample orderItemExample = new TbOrderItemExample();
            TbOrderItemExample.Criteria criteria1 = orderItemExample.createCriteria();
            criteria1.andOrderIdEqualTo(order.getOrderId());
            List<TbOrderItem> orderItemList = orderItemMapper.selectByExample(orderItemExample);
            orderResult.setOrderItemList(orderItemList);

            orderResultList.add(orderResult);
        }
        return orderResultList;
    }

    //删除订单(退款)
    @Override
    public void deleteOrderItemById(String id) {
        orderItemMapper.deleteByPrimaryKey(Long.valueOf(id));
    }

    @Override
    public List<TbOrder> selectOrderList(String message) {
        TbOrderExample tbOrderExample = new TbOrderExample();
        TbOrderExample.Criteria criteria = tbOrderExample.createCriteria();
        criteria.andOutTradeNoEqualTo(message);
        List<TbOrder> orderList = orderMapper.selectByExample(tbOrderExample);
        return orderList;
    }
}
